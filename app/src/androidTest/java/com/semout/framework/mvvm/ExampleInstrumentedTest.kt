package com.semout.framework.mvvm

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
//        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
//        assertEquals("com.mindorks.framework.mvvm", appContext.packageName)
        val obj = JSONObject()

        try {
            val jsres = "[{\"PTKP\":\"TK/3\"}]"
            val js = JSONArray(jsres)
            val obj2 = JSONObject()
            obj2.put("student", js.toString())
            println(obj2)
        } catch (e: JSONException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

    }
}
