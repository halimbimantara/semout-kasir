package com.semout.framework.mvvm.ui.produk.features.produk

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.github.dhaval2404.imagepicker.ImagePicker
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.data.model.local.db.KategoriProduk
import com.semout.framework.mvvm.data.model.local.db.Kemasan
import com.semout.framework.mvvm.data.model.local.db.Satuan
import com.semout.framework.mvvm.databinding.AddProdukScreenNewBinding
import com.semout.framework.mvvm.ui.kasir.BarcodeScannerActivity
import com.semout.framework.mvvm.ui.produk.features.AddKategoriProdukActivity
import com.semout.framework.mvvm.ui.produk.features.AddKemasanProdukActivity
import com.semout.framework.mvvm.ui.produk.features.AddSatuanProdukActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.ProdukViewModel
import com.semout.framework.mvvm.utils.ext.*
import com.skydoves.balloon.ArrowOrientation
import com.skydoves.balloon.Balloon
import com.skydoves.balloon.BalloonAnimation
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File


class AddProdukActivity : BaseActivity() {

    var listKategori: ArrayList<KategoriProduk?>? = null
    var listSatuan: ArrayList<Satuan?>? = null
    var listKemasan: ArrayList<Kemasan?>? = null
    private lateinit var balloon: Balloon
    private val viewModel by viewModel<ProdukViewModel>()
    lateinit var binding: AddProdukScreenNewBinding
    var idkat: Int = 0
    var isiK1: Int = 1
    var isiK2: Int = 0
    var idkemasan: Int = 0
    var idsatuan: Int = 0
    var inberat: Double = 0.0
    var inHgjual: Double = 0.0
    var ibarcode: String = ""
    var path_img: String = ""

    private val type_add by lazy {
        intent.getIntExtra(EXTRA_TYPE_ADD, 0)
    }

    private val namaProduk by lazy {
        intent.getStringExtra(EXTRA_TYPE_ADD_NAMA_PRODUK)
    }

    private val hargaProduk by lazy {
        intent.getStringExtra(EXTRA_TYPE_ADD_HARGA_PRODUK)
    }
    private val idProdTemp by lazy {
        intent.getIntExtra(EXTRA_TYPE_ADD_ID_TEMP, 0)
    }

    companion object {
        var EXTRA_TYPE_ADD = "extra_type"
        var EXTRA_TYPE_ADD_NAMA_PRODUK = "extra_add_nama_produk"
        var EXTRA_TYPE_ADD_HARGA_PRODUK = "extra_add_harga_produk"
        var EXTRA_TYPE_ADD_ID_TEMP = "extra_add_id_temp"//for del temp if success add
        fun newIntent(context: Context, tipe: Int): Intent {
            val intent =
                Intent(context, AddProdukActivity::class.java).putExtra(EXTRA_TYPE_ADD, tipe)
            return intent
        }

        fun newIntentAddDaftarHarga(
            context: Context,
            tipe: Int,
            nama_produk: String,
            harga: String,
            id_temp: Int
        ): Intent {
            val intent =
                Intent(context, AddProdukActivity::class.java)
                    .putExtra(EXTRA_TYPE_ADD, tipe)
                    .putExtra(EXTRA_TYPE_ADD_ID_TEMP, id_temp)
                    .putExtra(
                        EXTRA_TYPE_ADD_NAMA_PRODUK,
                        nama_produk
                    )
                    .putExtra(
                        EXTRA_TYPE_ADD_HARGA_PRODUK,
                        harga
                    )
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = AddProdukScreenNewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUi()
        action()
        initData()
        observeChange()
    }

    private fun initUi() {
        balloon = Balloon.Builder(this)
            .setLayout(R.layout.info_kemasan_k2)
            .setArrowSize(10)
            .setArrowOrientation(ArrowOrientation.BOTTOM)
            .setArrowPosition(0.5f)
            .setWidthRatio(0.55f)
            .setTextColor(ContextCompat.getColor(this, R.color.white))
            .setHeight(190)
            .setCornerRadius(4f)
            .setBackgroundColor(ContextCompat.getColor(this, R.color.black))
            .setBalloonAnimation(BalloonAnimation.FADE)
            .setLifecycleOwner(this)
            .build()
        binding.BackBtn.setOnClickListener {
            finish()
        }
//        binding.etBarcode.isEnabled = false
        binding.EtMaxStok.OnTextChangedListener {
            if (it.isNotEmpty()) {
                val x: Int = it.toInt()
                val total = (x * 10) / 100
                binding.EtMinStok.setText(total.toString())
            } else {
                binding.EtMinStok.setText("")
            }
        }

        if (type_add == 2) {//from daftar temp
            binding.EtFullName.setText(namaProduk)
            binding.etHargaJual.setText(hargaProduk)
        }
    }

    private fun initData() {
        //load kategori
        viewModel.loadKemasan()
        viewModel.loadKategori()
        viewModel.loadSatuan()
        viewModel.getKategori()
        viewModel.getSatuan()
        viewModel.getKemasan()

        viewModel.kategori.observe(this) {
            loadSpinKategori(it)
        }
        viewModel.satuan.observe(this) {
            loadSpinSatuan(it)
        }

        viewModel.kemasan.observe(this) {
            loadSpinKemasan(it)
        }
        viewModel.mTrigerSave.observe(this) {
            hideProgress()
            if (it) {
                showSnackbarMessage("Berhasil Menyimpan")
                binding.etBarcode.setText("")
                binding.EtFullName.setText("")
                binding.EtMinStok.setText("")
                binding.EtMaxStok.setText("")

                if (type_add == 1) {
                    val handler = Handler()
                    handler.postDelayed({ // Do something after 5s = 5000ms
                        finish()
                    }, 2000)
                } else if (type_add == 2) {
                    //delete temp
                    viewModel.delProdukTempById(idProdTemp)
                }
//                val handler = Handler()
            } else {
                showSnackbarError("Gagal Menyimpan")
            }
        }
    }

    fun action() {
        actionCheckedKemasan()
        binding.EtKemasanBeliK1.setText("1")
        binding.EtKemasanBeliK1.isEnabled = false
        binding.foto.setOnClickListener {
            ImagePicker.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }
        binding.SpinKategori.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                idkat = listKategori!![i]!!.id
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }

        //satuan jual
        binding.SpKemasanJual.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                idsatuan = listSatuan!![i]!!.id
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
//
        binding.SpinNamaKemasanBeli.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                println("pos $i")
                idkemasan = listKemasan!![i]!!.id
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        binding.etBarcode.setOnClickListener {
            val i = Intent(this, BarcodeScannerActivity::class.java)
            startActivityForResult(i, 2)
        }
        binding.addKategori.setOnClickListener {
            val i = Intent(this, AddKategoriProdukActivity::class.java)
            startActivity(i)
        }

        binding.addSatuanKp.setOnClickListener {
            val i = Intent(this, AddSatuanProdukActivity::class.java)
            startActivity(i)
        }
        binding.addKemasan.setOnClickListener {
            val i = Intent(this, AddKemasanProdukActivity::class.java)
            startActivity(i)
        }

        binding.BtnAddProduk.setOnClickListener {
            inHgjual = if (binding.etHargaJual.EtToString()
                    .isEmpty()
            ) 0.0 else binding.etHargaJual.EtToString().toDouble()
            showProgress()
            val minStok: Int = if (binding.EtMinStok.text!!.isNotEmpty()) Integer.parseInt(
                binding.EtMinStok.text.toString()
            ) else 0
            val maxStok: Int = if (binding.EtMaxStok.text!!.isNotEmpty()) Integer.parseInt(
                binding.EtMaxStok.text.toString()
            ) else 0
            val k2: Int = if (binding.EtKemasanBeliK2.EtToString().isNotEmpty()) Integer.parseInt(
                binding.EtMaxStok.text.toString()
            ) else 0
            viewModel.addProduk(
                ibarcode,
                binding.EtFullName.text.toString(),
                minStok,
                maxStok,
                idkat,
                idsatuan,
                idkemasan,
                k2,
                inberat,
                inHgjual,
                binding.etLetakRak.text.toString(),
                path_img
            )
        }

    }

    private fun actionCheckedKemasan() {
        //default is k1
        binding.SpinKemasank1.isChecked = true

        binding.labelIsiKemasan2.setOnClickListener {
            balloon.showAlignTop(it)
        }
        binding.SpinKemasank1.setOnCheckedChangeListener { buttonView, isChecked ->
            run {
                //setkemasan beli
                if (isChecked) {
                    binding.SpinKemasankp.isChecked = false
                    binding.SpKemasanJual.gone()
                    //k2 is optional
                    binding.SpinKemasanBelik2.isEnabled = true
                }else{

                }
            }
        }

        binding.SpinKemasankp.setOnCheckedChangeListener { _, isChecked ->
            run {
                setIsiKemasan2(isChecked)
                if (isChecked) {
                    /**
                     *  Kemasan Beli(untuk pembelian stok) Menampilkan K2,K1,Dan Kp
                    NB: isi kemasan hanya menampilkan isi k2 dan isi k1 berlaku sama terhadap satuan kemasan
                     */
                    binding.SpinKemasank1.isChecked = false
                    binding.contentSatuanKemasanKp.visible()
                    //set kemasan beli checked
                    binding.SpinKemasanBelik2.isChecked = true
                    binding.SpinKemasanBelikp.isChecked = true
                } else {
                    binding.SpinKemasanBelikp.isChecked = false
                    binding.contentSatuanKemasanKp.gone()
                }
            }
        }

        binding.SpinKemasanBelik2.setOnCheckedChangeListener { _, isChecked ->
            run {
                setEnableSatuanBeliK2(isChecked)
            }
        }
    }

    private fun setEnableSatuanBeliK2(checked: Boolean) {
        if (checked) {
            binding.labelnmKemasanK2Beli.visible()
            binding.addKemasan.visible()
        } else {
            binding.labelnmKemasanK2Beli.gone()
            binding.addKemasan.gone()
        }
    }

    private fun setIsiKemasan2(visible: Boolean) {
        binding.EtKemasanBeliK2.isEnabled = visible
        balloon.showAlignTop(binding.labelIsiKemasan2)
    }

    override fun observeChange() {
        observe(viewModel.mTrigerDelTemp, ::finishAdd)
    }

    private fun finishAdd(successDel: Boolean) {
        if (successDel) {
            val handler = Handler()
            handler.postDelayed({ // Do something after 5s = 5000ms
                finish()
            }, 2000)
        }
    }

    private fun loadSpinSatuan(mlistSatuan: List<Satuan>) {
        listSatuan = ArrayList()
        if (mlistSatuan.isNotEmpty()) {
            listSatuan!!.addAll(mlistSatuan)
            val listSpinner: MutableList<String> = ArrayList()
            for (i in 0 until listSatuan!!.size) {
                listSpinner.add(listSatuan!![i]!!.nama)
            }
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item, listSpinner
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.SpKemasanJual.adapter = adapter
        }
    }

    private fun loadSpinKategori(mlistKat: List<KategoriProduk>) {
        listKategori = ArrayList()
        if (mlistKat.isNotEmpty()) {
            listKategori!!.addAll(mlistKat)
            val listSpinner: MutableList<String> = ArrayList()
            for (i in 0 until listKategori!!.size) {
                listSpinner.add(listKategori!![i]!!.nama_kategori)
            }
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item, listSpinner
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.SpinKategori.adapter = adapter
        }
        binding.progressBarLoading.visibility = View.GONE
        binding.SpinKategori.visibility = View.VISIBLE
    }

    private fun loadSpinKemasan(mlistKat: List<Kemasan>) {
        listKemasan = ArrayList()
        if (mlistKat.isNotEmpty()) {
            listKemasan!!.addAll(mlistKat)
            val listSpinner: MutableList<String> = ArrayList()
            for (i in 0 until listKemasan!!.size) {
                listSpinner.add(mlistKat[i].namaKemasan)
            }
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item, listSpinner
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.SpinNamaKemasanBeli.adapter = adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === 2) {
            val barcode: String? = data?.getStringExtra(BarcodeScannerActivity.BARCODE_TYPE)
            ibarcode = barcode ?: ""
            binding.etBarcode.setText(barcode)
        }
        when (resultCode) {
            Activity.RESULT_OK -> {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data
                binding.foto.setImageURI(fileUri)
                //You can get File object from intent
                val file: File = ImagePicker.getFile(data)!!
                //You can also get File Path from intent
                path_img = ImagePicker.getFilePath(data)!!
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
