package com.semout.framework.mvvm.ui.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.databinding.FragmentsCsBinding

class CustomerService : BaseFragment() {
    override fun observeChange() {}
    lateinit var binding: FragmentsCsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentsCsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}