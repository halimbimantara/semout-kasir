package com.semout.framework.mvvm.utils

import com.semout.framework.mvvm.utils.ProjectUtils.getTimestamp
import com.semout.framework.mvvm.utils.TransaksiType.KODE_BELI
import com.semout.framework.mvvm.utils.TransaksiType.KODE_JUAL

object KasirHelper {

    /**
     * BL +TIMESTAMP
     */
    fun getKodeTrxPembelian(): String {
        return KODE_BELI + getTimestamp()
    }

    fun getKodeTrxPenjualan(): String {
        return KODE_JUAL + getTimestamp()
    }

    fun splitTemporary(input: String): List<String> {
        return if (input.contains("="))
            input.split("=", ignoreCase = true).toList()
        else
            emptyList()
    }

}