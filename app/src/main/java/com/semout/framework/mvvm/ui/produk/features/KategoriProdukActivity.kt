package com.semout.framework.mvvm.ui.produk.features

import android.content.Intent
import android.os.Bundle
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import kotlinx.android.synthetic.main.produk_list_screen.*

class KategoriProdukActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.produk_list_screen)
        initUi()
        action()
        observeChange()
    }

    private fun initUi() {
        textLabelToolbar.setText(getString(R.string.label_kategori_produk))
    }

    fun action() {
        BtnAddProduk.setOnClickListener {
            val i = Intent(this@KategoriProdukActivity, AddKategoriProdukActivity::class.java)
            startActivity(i)
        }
    }

    override fun observeChange() {
    }
}