package com.semout.framework.mvvm.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * halimc_comrade 22/03/20.
 */
data class BaseResponse<T>(
    @Expose @SerializedName("status")
    val status: Boolean,

    @Expose @SerializedName("message")
    val message: String,
    @Expose
    @SerializedName("data")
    val data: T
)

data class BaseResponses(
    @Expose
    @SerializedName("status")
    val status: Int,
    @Expose
    @SerializedName("message")
    val message: String
)

data class BasicResponse(
    @Expose
    @SerializedName("status")
    val status: Int,
    @Expose
    @SerializedName("success")
    val success: Boolean,
    @Expose
    @SerializedName("message")
    val message: String
)

data class ResponseRegister(
    @Expose @SerializedName("status")
    val status: Int,
    @Expose @SerializedName("message")
    val message: String,
    @Expose @SerializedName("success")
    val success: Boolean,
    @Expose
    @SerializedName("is_registered")
    val isregistered: Boolean
)

data class ResponseHandling(val success: Boolean,val event: String,val message: String)