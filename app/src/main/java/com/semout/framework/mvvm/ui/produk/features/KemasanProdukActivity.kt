package com.semout.framework.mvvm.ui.produk.features

import android.os.Bundle
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import kotlinx.android.synthetic.main.add_kemasan_produk_screen.*
import kotlinx.android.synthetic.main.produk_list_screen.textLabelToolbar

class KemasanProdukActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_kemasan_produk_screen)
        initUi()
        action()
        observeChange()
    }

    private fun initUi() {
        textLabelToolbar.setText(getString(R.string.label_kemasan_produk))
    }

    fun action() {
        BtnSaveKemasan.setOnClickListener {
          showProgress()

        }
    }

    override fun observeChange() {
    }
}