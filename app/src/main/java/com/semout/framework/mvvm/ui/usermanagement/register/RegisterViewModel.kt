package com.semout.framework.mvvm.ui.usermanagement.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.semout.framework.mvvm.core.BaseViewModel
import com.semout.framework.mvvm.data.model.ResponseHandling
import com.semout.framework.mvvm.data.repository.Repository
import com.semout.framework.mvvm.utils.CaseType.API_SUCCESS
import com.semout.framework.mvvm.utils.NetworkHelper
import com.semout.framework.mvvm.utils.SingleLiveEvent
import com.semout.framework.mvvm.utils.ext.addTo
import com.semout.framework.mvvm.utils.rx.SchedulerProvider
import kotlinx.coroutines.launch

class RegisterViewModel(
    private val appRepository: Repository,
    private val schedulerProvider: SchedulerProvider,
    private val networkHelper: NetworkHelper
) : BaseViewModel() {
    val errorMessage = SingleLiveEvent<String>()
    val successMessage = SingleLiveEvent<String>()
    val responseData = SingleLiveEvent<ResponseHandling>()

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    fun register(fullname: String, email: String, phone: String, uname: String, pwd: String) {
        _loading.postValue(true)
        appRepository.register(fullname, uname, email, phone, pwd, "-")
            .subscribeOn(schedulerProvider.io())
            .subscribe({
                _loading.postValue(false)
                successMessage.postValue(it.message)
                if (it.status == API_SUCCESS) {
                    responseData.postValue(ResponseHandling(true, "register", it.message))
                } else {
                    responseData.postValue(ResponseHandling(false, "register", it.message))
                }
            }, {
                errorMessage.postValue(networkHelper.CatchErrors(it, "register"))
                _loading.postValue(false)
            })
            .addTo(compositeDisposable)
    }
}