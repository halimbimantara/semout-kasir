package com.semout.framework.mvvm

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import com.facebook.stetho.Stetho
import com.orhanobut.hawk.Hawk
import com.semout.framework.mvvm.di.*
import io.github.inflationx.calligraphy3.CalligraphyConfig
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    private val calConfig: CalligraphyConfig by inject()

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(networkModule)
            modules(databaseModule)
            modules(persistenceModule)
            modules(repositoryModule)
            modules(appModule)
            modules(viewModelModule)
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        Stetho.initializeWithDefaults(this)
        Hawk.init(applicationContext).setLogInterceptor { message ->
//            if (BuildConfig.DEBUG) {
//                Log.d("Hawk", message)
//            }
        }.build()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}


