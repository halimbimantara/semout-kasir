package com.semout.framework.mvvm.data.local.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.semout.framework.mvvm.data.model.local.db.Kemasan;
import com.semout.framework.mvvm.data.model.local.db.Satuan;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface SatuanProdukDao {

    @Delete
    void delete(Satuan user);

    @Query("SELECT * FROM satuan WHERE nama LIKE :name LIMIT 1")
    Single<Satuan> findByName(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Satuan satuan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Satuan> satuans);

    @Query("SELECT * FROM satuan")
    Flowable<List<Satuan>> loadAll();

    @Query("SELECT * FROM satuan WHERE id IN (:satuanIds)")
    Flowable<List<Satuan>> loadAllById(List<Integer> satuanIds);
}
