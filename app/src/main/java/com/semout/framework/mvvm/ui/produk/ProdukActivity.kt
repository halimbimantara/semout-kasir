package com.semout.framework.mvvm.ui.produk

import android.content.Intent
import android.os.Bundle
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.ui.produk.features.DaftarHargaActivity
import com.semout.framework.mvvm.ui.produk.features.KategoriProdukActivity
import com.semout.framework.mvvm.ui.produk.features.KemasanProdukActivity
import com.semout.framework.mvvm.ui.produk.features.ProdukStokMainActivity
import com.semout.framework.mvvm.ui.produk.features.pembelian.AddPembelianActivity
import com.semout.framework.mvvm.ui.produk.features.pembelian.PembelianActivity
import kotlinx.android.synthetic.main.activity_produk.*

class ProdukActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_produk)
        action()
        observeChange()
    }

    fun action() {
        cv_menu_subproduk.setOnClickListener {
            val i = Intent(this@ProdukActivity, ProdukStokMainActivity::class.java)
            startActivity(i)
        }

        cv_menu_subkategori.setOnClickListener {
            val i = Intent(this@ProdukActivity, DaftarHargaActivity::class.java)
            startActivity(i)
        }

        cv_menu_subkemasan.setOnClickListener {
            val i = Intent(this@ProdukActivity, KemasanProdukActivity::class.java)
            startActivity(i)
        }

        cv_menu_subpembelian.setOnClickListener {
            val i = Intent(this@ProdukActivity, PembelianActivity::class.java)
            startActivity(i)
        }
    }

    override fun observeChange() {
    }
}