package com.semout.framework.mvvm.data.api

import com.semout.framework.mvvm.data.model.BaseResponse
import com.semout.framework.mvvm.data.model.BaseResponses
import com.semout.framework.mvvm.data.model.BasicResponse
import com.semout.framework.mvvm.data.model.api.UserLogin
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

@JvmSuppressWildcards
interface Api {
    @FormUrlEncoded
    @POST("login/login_api")
    fun login(
        @Field("username") uname: String?, @Field("password") pwd: String?, @Field("uuid") imei: String?
    ): Observable<BaseResponse<UserLogin>>

    @FormUrlEncoded
    @POST("register/api_register")
    fun register(
        @Field("nama") fname: String?,
        @Field("username") uname: String?,
        @Field("email") email: String?,
        @Field("password") pwd: String?,
        @Field("no_hp") phone: String?,
        @Field("alamat") alamat: String?
    ): Observable<BaseResponses>
}