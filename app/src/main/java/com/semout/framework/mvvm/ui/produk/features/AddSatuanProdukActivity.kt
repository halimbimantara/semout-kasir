package com.semout.framework.mvvm.ui.produk.features

import android.os.Bundle
import android.os.Handler
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.KemasanViewModel
import kotlinx.android.synthetic.main.add_kemasan_produk_screen.*
import org.koin.android.viewmodel.ext.android.viewModel

class AddSatuanProdukActivity : BaseActivity() {
    private val mviewModel by viewModel<KemasanViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_satuan_produk_screen)
        initUi()
        action()
        observeChange()
    }

    private fun initUi() {
//        textLabelToolbar.setText(getString(R.string.label_kemasan_produk))
    }

    fun action() {
       BackBtn.setOnClickListener {
           finish()
       }
        BtnSaveKemasan.setOnClickListener {
            showProgress()
            mviewModel.addSatuan(EtKemasan.text.toString(), EtKemasanSingkatan.text.toString())
        }

        mviewModel.mTriger.observe(this, { success ->
            hideProgress()
            if (success) {
                EtKemasan.setText("")
                showSnackbarMessage("Berhasil Menyimpan")
                val handler = Handler()
                handler.postDelayed({ // Do something after 5s = 5000ms
                    finish()
                }, 2000)
            } else {
                showSnackbarMessage("Gagal Menyimpan")
            }
        })
    }

    override fun observeChange() {
    }
}