package com.semout.framework.mvvm.ui.produk.features

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.semout.framework.mvvm.utils.common_adapter.ViewPagerAdapter
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.ui.produk.fragment.StokFragment
import com.semout.framework.mvvm.ui.produk.fragment.SuplierFragment
import kotlinx.android.synthetic.main.produkstok_main_screen.*

class ProdukStokMainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.produkstok_main_screen)
        initToolbar()
        initUi()
        action()
        observeChange()
    }

    private fun initToolbar() {

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white)

        if (toolbar.navigationIcon != null) {
            toolbar.navigationIcon?.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.md_white_1000
                ), PorterDuff.Mode.SRC_ATOP
            )
        }

        toolbar.title = "Produk & Stok"

        try {
            toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.md_white_1000))
        } catch (e: Exception) {
            Log.e("TEAMPS", "Can't set color.")
        }

        try {
            setSupportActionBar(toolbar)
        } catch (e: Exception) {
            Log.e("TEAMPS", "Error in set support action bar.")
        }

        try {
            if (supportActionBar != null) {
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
            }
        } catch (e: Exception) {
            Log.e("TEAMPS", "Error in set display home as up enabled.")
        }

    }

    private fun initUi() {
        setupViewPager(viewPager)

        tab_layout.setupWithViewPager(viewPager)
        tab_layout.setTabTextColors(
            ContextCompat.getColor(this, R.color.md_white_1000),
            ContextCompat.getColor(this, R.color.md_white_1000)
        )
//        setupTabIcons()

        try {

            // set icon color pre-selected
            val tab1 = tab_layout.getTabAt(0)
            if (tab1 != null) {
                tab1.setIcon(R.drawable.ic_baseline_list_harga_24)
                if (tab1.icon != null) {
                    tab1.icon?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
                }
            }

            val tab2 = tab_layout.getTabAt(1)
            if (tab2 != null) {
                tab2.setIcon(R.drawable.ic_baseline_supervisor_account_24)
                if (tab2.icon != null) {
                    tab2.icon?.setColorFilter(
                        ContextCompat.getColor(this, R.color.md_grey_200),
                        PorterDuff.Mode.SRC_IN
                    )
                }
            }

            tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    if (tab.icon != null) {
                        tab.icon?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {
                    if (tab.icon != null) {
                        tab.icon?.setColorFilter(
                            ContextCompat.getColor(
                                this@ProdukStokMainActivity,
                                R.color.md_grey_200
                            ), PorterDuff.Mode.SRC_IN
                        )
                    }
                }

                override fun onTabReselected(tab: TabLayout.Tab) {
                    if (tab.icon != null) {
                        tab.icon?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
                    }
                }
            })

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun action() {

    }

    override fun observeChange() {
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)

        val fragment1 = StokFragment()
//        fragment1.setType("Stok")

        val fragment2 = SuplierFragment()
//        fragment2.setType("B")


        adapter.addFragment(fragment1, "Stok")
        adapter.addFragment(fragment2, "Suplier")
        viewPager.adapter = adapter
    }
}