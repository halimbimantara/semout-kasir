package com.semout.framework.mvvm.ui.produk.features.pembelian

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.data.model.local.db.MasterSuplier
import com.semout.framework.mvvm.data.model.local.db.Produk
import com.semout.framework.mvvm.data.model.local.db.TrxPembelian
import com.semout.framework.mvvm.databinding.AddPembelianScreenBinding
import com.semout.framework.mvvm.ui.kasir.BarcodeScannerActivity
import com.semout.framework.mvvm.ui.produk.features.produk.AddProdukActivity
import com.semout.framework.mvvm.ui.produk.features.ListProdukActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.PembelianViewModel
import com.semout.framework.mvvm.utils.NumberUtils
import com.semout.framework.mvvm.utils.NumberUtils.convertNumber
import com.semout.framework.mvvm.utils.ProjectUtils.getDefaultFormatUser
import com.semout.framework.mvvm.utils.TimeFormat.DATE_FORMAT_DEFAULT
import com.semout.framework.mvvm.utils.TimeFormat.DATE_FORMAT_REVIEW
import com.semout.framework.mvvm.utils.ext.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class AddPembelianActivity : BaseActivity() {
    var listSuplier: ArrayList<MasterSuplier?>? = null
    var listProduk: ArrayList<Produk?>? = null
    private val mviewModel by viewModel<PembelianViewModel>()
    lateinit var binding: AddPembelianScreenBinding
    var idkat: Int = 0

    private var inDateBirth: String? = ""
    var idSuplier: Int = 0
    var idProduk: Int = 0
    var inputK2: Int = 0
    var inputKP: Int = 0
    var nmProduk: String = ""
    var ibarcode: String = ""
    var isEdit: Boolean = false

    var checkK1: Boolean = false
    var checkK2: Boolean = false
    var checkK3: Boolean = false

    private val _idSuplier by lazy {
        intent.getIntExtra(EXTRA_ID_SUPLIER, 0)
    }
    private val _namasuplier by lazy {
        intent.getStringExtra(EXTRA_NAMA_SUPLIER)
    }
    private val action_type by lazy {////edit/add
        intent.getStringExtra(EXTRA_ACTION_TYPE)
    }

    private val kode_trx by lazy {
        intent.getStringExtra(EXTRA_KD_TRX_BELI)
    }
    private val id_pembelian by lazy {
        intent.getIntExtra(EXTRA_ID_PEMBELIAN, 0)
    }

    private var calendar: Calendar? = null

    companion object {
        const val EXTRA_REQ_ID_PRODUK = 22
        const val EXTRA_ID_PRODUK = "extra_product_id"
        const val EXTRA_ISI_K2 = "extra_product_k2"
        const val EXTRA_KP = "extra_product_kp"
        const val EXTRA_NAMA_PRODUK = "extra_product_name"
        const val EXTRA_ACTION_TYPE = "extra_action_type"//add / edit
        const val EXTRA_ID_SUPLIER = "extra_id_suplier"
        const val EXTRA_ID_PEMBELIAN = "extra_id_pembelian"
        const val EXTRA_NAMA_SUPLIER = "extra_nama_suplier"
        const val EXTRA_KD_TRX_BELI = "extra_kode_trx_beli"
        fun newIntent(
            context: Context,
            idPembelian: Int,
            idsuplier: Int,
            namaSuplier: String,
            kd_trx_beli: String,
            action: String
        ): Intent {
            val intent =
                Intent(context, AddPembelianActivity::class.java)
                    .putExtra(EXTRA_ID_PEMBELIAN, idPembelian)
                    .putExtra(EXTRA_ACTION_TYPE, action)
                    .putExtra(EXTRA_ID_SUPLIER, idsuplier)
                    .putExtra(EXTRA_NAMA_SUPLIER, namaSuplier)
                    .putExtra(EXTRA_KD_TRX_BELI, kd_trx_beli)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = AddPembelianScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUi()
        action()
        initData()
    }

    private fun initUi() {
        calendar = Calendar.getInstance()
        if (action_type.equals("add")) {
            binding.BtnAddProduk.text = "Simpan"
        } else {
            binding.BtnAddProduk.text = "Edit"
            isEdit = true
        }
        checkK1 = true
    }

    private fun initData() {
        //load kategori
        mviewModel.loadSuplier()
        mviewModel.getSuplier()
        mviewModel.getProduk()
        mviewModel.loadProduk()
        binding.EtTglTrx.setText(getDefaultFormatUser().toString())
    }

    fun getSuplierPos(IdSup: Int): Int {
        var pos: Int = 0
        for (i in listSuplier!!.indices) {
            if (listSuplier!![i]?.id_suplier!!.equals(IdSup)) {
                pos = i
            }
        }
        return pos
    }

    fun getProdukPos(IdSup: Int): Int {
        var pos: Int = 0
        for (i in listProduk!!.indices) {
            if (listProduk!![i]?.id!! == IdSup) {
                pos = i
            }
        }
        return pos
    }

    @SuppressLint("SetTextI18n")
    fun action() {
        binding.BackBtn.setOnClickListener {
            finish()
        }
        val dateListener =
            DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, month: Int, dayOfMonth: Int ->
                calendar!!.set(Calendar.YEAR, year)
                calendar!!.set(Calendar.MONTH, month)
                calendar!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val sdf = SimpleDateFormat(DATE_FORMAT_REVIEW, Locale("Asia/Jakarta"))
                val sdfdefault = SimpleDateFormat(DATE_FORMAT_DEFAULT, Locale("Asia/Jakarta"))
                inDateBirth = sdfdefault.format(calendar!!.time)
                binding.EtTglTrx.setText(sdf.format(calendar!!.time))
            }
        binding.imvDate.setOnClickListener {
            val mdialog = DatePickerDialog(
                this@AddPembelianActivity,
                dateListener,
                calendar!!.get(Calendar.YEAR),
                calendar!!.get(Calendar.MONTH),
                calendar!!.get(Calendar.DAY_OF_MONTH)
            )
            mdialog.show()
        }
        binding.EtSuplier.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                if (i > 0) {
                    idSuplier = listSuplier!!.get(i)!!.id_suplier
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }

        binding.SpinNamaProduk.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                if (i > 0) {
                    idProduk = listProduk!![i]!!.id
                    nmProduk = listProduk!![i]!!.namaProduk
                    inputK2 = listProduk!![i]!!.k2
                    inputKP = listProduk!![i]!!.idkemasan

                    binding.TvInfoK2.text = "Total K1=${listProduk!![i]!!.k2}"
                    binding.TvInfoK2.gone()

                    if (inputK2 > 0) {
                        binding.SpinKemasan.isChecked = true
                    } else {
                        binding.SpinKemasan.isEnabled = false
                    }

                    /**
                     * jika yg dicentang k2 maka qty * isi k2
                     * jika
                     */
                    if (inputKP > 0) {
                        //centang
                        binding.SpinKemasankp.isChecked = true
                    } else {
                        binding.SpinKemasankp.isEnabled = false
                    }

                    //get data pembelian terkahir by id produk
                    getHargaBeliLast(idProduk)
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }

        binding.addProduk.setOnClickListener {
            startActivity(AddProdukActivity.newIntent(this, 1))
        }
        binding.BtnAddProduk.setOnClickListener {
            validasi()
        }

        binding.imvSearchproduk.setOnClickListener {
            startActivityForResult(
                ListProdukActivity.newIntent(this@AddPembelianActivity),
                EXTRA_REQ_ID_PRODUK
            )
        }

        /**
         * JIka centang k2 maka beli k2
         * qty k1 * k2
         * K1 = (jumlah brg * isi K2)
        KP = (jumlah brg * isi K2) + (jumlah brg * isi K1)
         */
        binding.EtQty.OnTextChangedListener {
            if (it.isNotEmpty()) {
                if (binding.EtHargaBeli.EtToString().isNotEmpty()) {
                    if (inputK2 > 0) {
                        val totalK1 = it.toInt() * inputK2
                        binding.TvInfoK2.text = "Total K1=${totalK1}"
                    }
                    val subtotal =
                        (binding.EtHargaBeli.EtToString().toInt() * it.toInt()).toString()
                    binding.TvSubtotal.text =
                        "Subtotal :${NumberUtils.convertNumber(subtotal)}"
                } else {
                    binding.TvSubtotal.text = "Subtotal : 0"
                    binding.EtHargaBeli.error = "Harap Isi"
                }
            } else {
                binding.TvSubtotal.text = "Subtotal : 0"
            }
        }

        binding.EtHargaBeli.OnTextChangedListener {
            if (it.isNotEmpty()) {
                if (binding.EtQty.EtToString().isNotEmpty()) {
                    val subtotal =
                        (it.toInt() * binding.EtQty.EtToString().toInt()).toString()
                    binding.TvSubtotal.text =
                        "Subtotal : ${NumberUtils.convertNumber(subtotal)}"
                } else {
                    binding.TvSubtotal.text = "Subtotal : 0"
                }
            } else {
                binding.TvSubtotal.text = "Subtotal : 0"
            }
        }
        //checkbox
        binding.SpinKemasan.setOnCheckedChangeListener { buttonView, isChecked ->
            checkK2 = isChecked
            if (isChecked) {
                binding.SpinKemasank1.isChecked = false
                binding.SpinKemasankp.isChecked = false
                binding.TvInfoK2.visible()
                showSnackbarMessage("Pastikan Harga Beli Untuk Kemasan (k2)(sak/palet/dus dst)")
            } else {
                binding.TvInfoK2.gone()
            }
        }

        binding.SpinKemasank1.setOnCheckedChangeListener { buttonView, isChecked ->
            checkK1 = isChecked
            binding.SpinKemasan.isChecked = false
            binding.SpinKemasankp.isChecked = false
        }

        binding.SpinKemasankp.setOnCheckedChangeListener { buttonView, isChecked ->
            checkK3 = isChecked
            if (isChecked) {
                binding.SpinKemasan.isChecked = false
                binding.SpinKemasank1.isChecked = false
            }
        }


    }

    private fun getHargaBeliLast(idProduk: Int) {
        mviewModel.getLastTrxPembelian(idProduk)
    }

    private fun validasi() {
        val info = "Harap Isi"
        if (idProduk == -1 || idProduk == 0) {
            showSnackbarError("Harap pilih produk ")
        } else if (binding.EtHargaBeli.EtToString().isEmpty()) {
            binding.EtHargaBeli.error = info
        } else if (binding.EtQty.EtToString().isEmpty()) {
            binding.EtQty.error = info
        } else if (kode_trx.toString().isEmpty()) {
            showSnackbarError("Kode transaksi belum ada")
        } else {
            if (isEdit) {
                mviewModel.editPembelian(
                    id_pembelian,
                    kode_trx.toString(),
                    idProduk,
                    nmProduk,
                    idSuplier,
                    binding.EtQty.EtToString().toInt(),
                    binding.EtHargaBeli.EtToString().toFloat(),
                    binding.EtPotonganNominal.EtToString(),
                    binding.EtPot.EtToString()
                )
            } else {
                //save
                val potDiskNominal = if (binding.EtPotonganNominal.EtToString()
                        .isEmpty()
                ) "0" else binding.EtPotonganNominal.EtToString()
                val potDisk = if (binding.EtPot.EtToString()
                        .isEmpty()
                ) "0" else binding.EtPot.EtToString()
                mviewModel.addPembelianTemp(
                    kode_trx.toString(),
                    idProduk,
                    nmProduk,
                    idSuplier,
                    binding.EtQty.EtToString().toInt(),
                    binding.EtHargaBeli.EtToString().toFloat(),
                    potDiskNominal,
                    potDisk
                )
            }
        }
    }

    override fun observeChange() {
        observe(mviewModel.produk, ::loadSpinProduk)
        observe(mviewModel.suplier, ::loadSpinSuplier)
        observe(mviewModel.mTrigerSave, ::successSimpan)
        observe(mviewModel.trxPembelianByProduct, ::getLastPembelian)
    }

    @SuppressLint("SetTextI18n")
    private fun getLastPembelian(response: List<TrxPembelian>) {
        //hide loading
        if (!response.isNullOrEmpty()) {
            val _produkBeli = response[0]
            val _convertHarga = convertNumber(_produkBeli.harga.toString().removeDoubleValue())
            binding.TvhargaBeliLast.text = "Harga sebelumnya Rp.${_convertHarga}"
        }
    }

    private fun successSimpan(any: Boolean) {
        if (any) {
            binding.EtHargaBeli.setText("")
            binding.EtQty.setText("")
            binding.EtPot.setText("")
            binding.EtPotonganNominal.setText("")
            showSnackbarMessage("Berhasil Menyimpan")
            val handler = Handler()
            handler.postDelayed({ // Do something after 5s = 5000ms
                finish()
            }, 2000)
        } else {
            showSnackbarError("Gagal Menyimpan")
        }
    }

    private fun loadSpinSuplier(mlistSuplier: List<MasterSuplier>) {
        listSuplier = ArrayList()
        val _pilih = MasterSuplier()
        _pilih.nama_suplier = "Pilih Suplier"
        _pilih.id_suplier = 0
        listSuplier!!.add(_pilih)
        if (mlistSuplier.isNotEmpty()) {
            listSuplier!!.addAll(mlistSuplier)
            val listSpinner: MutableList<String> = ArrayList()
            for (i in 0 until listSuplier!!.size) {
                listSpinner.add(listSuplier!![i]!!.nama_suplier)
            }
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item, listSpinner
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.EtSuplier.adapter = adapter
        }
        binding.EtSuplier.setSelection(getSuplierPos(_idSuplier))
    }

    private fun loadSpinProduk(mlistProduk: List<Produk>) {
        listProduk = ArrayList()
        val _pilih = Produk()
        _pilih.namaProduk = "Pilih Produk"
        _pilih.id = 0
        listProduk!!.add(_pilih)

        if (mlistProduk.isNotEmpty()) {
            listProduk!!.addAll(mlistProduk)
            val listSpinner: MutableList<String> = ArrayList()
            for (i in 0 until listProduk!!.size) {
                listSpinner.add(listProduk!![i]!!.namaProduk)
            }
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item, listSpinner
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.SpinNamaProduk.adapter = adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2) {
            val barcode: String? = data?.getStringExtra(BarcodeScannerActivity.BARCODE_TYPE)
            ibarcode = barcode ?: ""
            binding.etBarcode.setText(barcode)
        } else if (requestCode == EXTRA_REQ_ID_PRODUK) {
            if (data?.getIntExtra(EXTRA_ID_PRODUK, 0) != null) {
                idProduk = data?.getIntExtra(EXTRA_ID_PRODUK, 0)!!
                binding.SpinNamaProduk.setSelection(getProdukPos(idProduk))
                nmProduk = data.getStringExtra(EXTRA_NAMA_PRODUK).toString()
                binding.SpinNamaProduk.setSelection(0)
                inputK2 = data.getIntExtra(EXTRA_ISI_K2, 0)
                inputKP = data.getIntExtra(EXTRA_KP, 0)

                binding.TvInfoK2.text = "Isi K2=${inputK2}"
                if (inputK2 > 0) {
                    binding.SpinKemasan.isChecked = true
                } else {
                    binding.SpinKemasan.isEnabled = false
                }

                /**
                 * jika yg dicentang k2 maka qty * isi k2
                 * jika
                 */
                if (inputKP > 0) {
                    //centang
                    binding.SpinKemasankp.isChecked = true
                } else {
                    binding.SpinKemasankp.isEnabled = false
                }
            }
        }//ignore
    }
}
