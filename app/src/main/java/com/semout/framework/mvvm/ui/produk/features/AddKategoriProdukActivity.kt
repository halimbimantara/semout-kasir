package com.semout.framework.mvvm.ui.produk.features

import android.os.Bundle
import android.os.Handler
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.KategoriViewModel
import kotlinx.android.synthetic.main.add_kat_produk_screen.*
import org.koin.android.viewmodel.ext.android.viewModel

class AddKategoriProdukActivity : BaseActivity() {
    private val mviewModel by viewModel<KategoriViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_kat_produk_screen)
        initUi()
        action()
        observeChange()
    }

    private fun initUi() {
        textLabelToolbar.setText(getString(R.string.label_kategori_produk))
    }

    fun action() {
        back_btn_verify.setOnClickListener {
            finish()
        }
        BtnAddKategori.setOnClickListener {
            showProgress()
            mviewModel.addKategori(etKategori.text.toString())
        }
        mviewModel.mTriger.observe(this, { success ->
            hideProgress()
            if (success) {
                etKategori.setText("")
                showSnackbarMessage("Berhasil Menyimpan")
                val handler = Handler()
                handler.postDelayed({ // Do something after 5s = 5000ms
                    finish()
                }, 2000)
            } else {
                showSnackbarMessage("Gagal Menyimpan")
            }
        })
    }

    override fun observeChange() {

    }
}