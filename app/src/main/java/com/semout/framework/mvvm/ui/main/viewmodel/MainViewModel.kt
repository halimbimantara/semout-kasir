package com.semout.framework.mvvm.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.semout.framework.mvvm.core.BaseViewModel
import com.semout.framework.mvvm.data.model.api.User
import com.semout.framework.mvvm.data.repository.Repository
import com.semout.framework.mvvm.utils.NetworkHelper
import com.semout.framework.mvvm.utils.Resource

class MainViewModel (
    private val mainRepository: Repository,
    private val networkHelper: NetworkHelper
) : BaseViewModel() {

    private val _users = MutableLiveData<Resource<List<User>>>()
    val users: LiveData<Resource<List<User>>>
        get() = _users

    init {
//        fetchUsers()
    }


}