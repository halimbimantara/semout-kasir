package com.semout.framework.mvvm.ui.produk.adapter

import android.content.Context
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.data.local.dao.ProdukDao
import de.codecrafters.tableview.SortableTableView
import de.codecrafters.tableview.model.TableColumnWeightModel
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter
import de.codecrafters.tableview.toolkit.SortStateViewProviders
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders


class SortableCarTableView(context: Context?, attributes: AttributeSet?, styleAttributes: Int) :
    SortableTableView<ProdukDao.InfoStok?>(context, attributes, styleAttributes) {
//    constructor(context: Context?) : this(context, null) {}
    constructor(context: Context?, attributes: AttributeSet?) : this(
        context,
        attributes,
    android.R.attr.listViewStyle
    ) {
    }

    init {
        val simpleTableHeaderAdapter = SimpleTableHeaderAdapter(
            context,
            "nm_produk",
            "stok",
            "max_stok",
            "min_stok",
            "letak_rak"
        )
        simpleTableHeaderAdapter.setTextColor(
            ContextCompat.getColor(
                context!!,
                R.color.grey_300
            )
        )
        headerAdapter = simpleTableHeaderAdapter
        val rowColorEven = ContextCompat.getColor(context, R.color.black)
        val rowColorOdd = ContextCompat.getColor(context, R.color.md_blue_300)
        setDataRowBackgroundProvider(
            TableDataRowBackgroundProviders.alternatingRowColors(
                rowColorEven,
                rowColorOdd
            )
        )
        headerSortStateViewProvider = SortStateViewProviders.brightArrows()
        val tableColumnWeightModel = TableColumnWeightModel(4)
        tableColumnWeightModel.setColumnWeight(0, 2)
        tableColumnWeightModel.setColumnWeight(1, 3)
        tableColumnWeightModel.setColumnWeight(2, 3)
        tableColumnWeightModel.setColumnWeight(3, 2)
        columnModel = tableColumnWeightModel
    }
}