package com.semout.framework.mvvm.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import java.net.ProtocolException

class NetworkHelper(private val context: Context) {

    fun isNetworkConnected(): Boolean {
        var result = false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val activeNetwork =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }
        return result
    }

    fun CatchErrors(e: Throwable, info: String?): String {
        var msg: String? = ""
        try {
            msg = JSONObject(
                (e as HttpException).response()!!.errorBody()!!.string()
            ).getString("message")
        } catch (e: ProtocolException) {
            msg = e.message
        } catch (ej: JSONException) {
            msg = e.message
        }catch (e:ClassCastException){
            msg=e.message
        }
        return if (msg!!.isEmpty()) "${e.message} $info" else msg
    }
}