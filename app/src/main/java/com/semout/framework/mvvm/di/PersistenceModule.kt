package com.semout.framework.mvvm.di

import com.semout.framework.mvvm.data.pref.AppPrefSource
import com.semout.framework.mvvm.data.pref.AppPrefSourceImpl
import org.koin.dsl.module

val persistenceModule = module {
    single {
        AppPrefSourceImpl(get())
    }
    single {
        AppPrefSource()
    }
}


