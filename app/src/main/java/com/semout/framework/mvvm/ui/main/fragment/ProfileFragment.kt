package com.semout.framework.mvvm.ui.main.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.databinding.FragmentsMyprofileBinding
import com.semout.framework.mvvm.ui.settings.activity.AppSettingActivity
import com.semout.framework.mvvm.ui.settings.activity.ProfilActivity
import com.semout.framework.mvvm.ui.settings.activity.SettingAlamatActivity

class ProfileFragment : BaseFragment() {
    override fun observeChange() {}
    lateinit var binding: FragmentsMyprofileBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentsMyprofileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionUi()
    }

    private fun actionUi() {
        binding.lladdAlamat.setOnClickListener {
            val intenr = Intent(requireContext(), SettingAlamatActivity::class.java)
            startActivity(intenr)
        }
        binding.llAppSetting.setOnClickListener {
            val intenr = Intent(requireContext(), AppSettingActivity::class.java)
            startActivity(intenr)
        }
        binding.llPesananSaya.setOnClickListener {
            startActivity(ProfilActivity.newIntent(requireContext()))
        }
    }

}