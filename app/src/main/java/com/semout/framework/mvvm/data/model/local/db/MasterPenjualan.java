/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.semout.framework.mvvm.data.model.local.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "master_penjualan")
public class MasterPenjualan {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "id_kasir")
    public int id_kasir;

    @ColumnInfo(name = "id_pelanggan")
    public int id_pelanggan;

    @ColumnInfo(name = "tipe_penjualan")
    public int tipe_penjualan;

    @ColumnInfo(name = "id_delivery")
    public int id_delivery;

    @ColumnInfo(name = "created_date")
    public String created_date;

    @ColumnInfo(name = "kd_trx_penjualan")
    public String kd_trx_penjualan;

    @ColumnInfo(name = "total_penjualan")
    public double total_penjualan;

    //diskon nota
    @ColumnInfo(name = "potongan_nota")
    public double potongan_nota;

}
