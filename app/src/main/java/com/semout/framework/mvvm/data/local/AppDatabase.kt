/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.semout.framework.mvvm.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.semout.framework.mvvm.data.local.dao.*
import com.semout.framework.mvvm.data.model.local.db.*
import com.semout.framework.mvvm.utils.DATABASE_NAME

/**
 * The Room database for this app
 */
@Database(
    entities = [Produk::class, Kemasan::class,
        KategoriProduk::class, UserLocal::class,
        Pelanggan::class, MasterPenjualan::class,
        MasterPembelian::class, TrxPembelian::class,TrxPembelianTemp::class,
        TrxPenjualan::class,TrxPenjualanTemp::class, MasterMargin::class,
        MasterSuplier::class, MasterRak::class,TrxSearchTemporary::class,
        Satuan::class],
    version = 20
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun produkDao(): ProdukDao
    abstract fun kategoriDao(): KategoriProdukDao
    abstract fun kemasanDao(): KemasanProdukDao
    abstract fun pelangganDao(): PelangganDao
    abstract fun masterMarginDao(): MasterMarginDao
    abstract fun masterPembelianDao(): MasterPembelianDao
    abstract fun trxpembelianDao(): TrxPembelianDao
    abstract fun trxpembelianTmpDao(): TrxPembelianTempDao
    abstract fun trxSearchTempDao(): TrxSearchTemp
    abstract fun masterPenjualanDao(): MasterPenjualanDao
    abstract fun trxpenjualanDao(): TrxPenjualanDao
    abstract fun trxpenjualanTmpDao(): TrxPenjualanTempDao
    abstract fun satuanDao(): SatuanProdukDao
    abstract fun suplierDao(): SuplierDao
    abstract fun masterRak(): MasterRakDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java, "app.db"
            ).fallbackToDestructiveMigration()
                .build()
    }

}
