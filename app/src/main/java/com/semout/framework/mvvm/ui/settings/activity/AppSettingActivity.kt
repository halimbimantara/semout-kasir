package com.semout.framework.mvvm.ui.settings.activity

import android.os.Bundle
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.databinding.AppSettingScreenBinding

class AppSettingActivity : BaseActivity() {

    lateinit var binding: AppSettingScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = AppSettingScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUi()
        action()
    }

    private fun initUi() {

    }

    fun action() {
        binding.backBtnVerify.setOnClickListener {
            finish()
        }
    }

    override fun observeChange() {
    }
}