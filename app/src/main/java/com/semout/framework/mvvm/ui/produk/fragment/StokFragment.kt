package com.semout.framework.mvvm.ui.produk.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.data.local.dao.ProdukDao
import com.semout.framework.mvvm.databinding.FragmentTabStokBinding
import com.semout.framework.mvvm.databinding.ItemStokProdukBinding
import com.semout.framework.mvvm.ui.kasir.BarcodeScannerActivity
import com.semout.framework.mvvm.ui.produk.adapter.ListStokAdapter
import com.semout.framework.mvvm.ui.produk.adapter.SortableCarTableView
import com.semout.framework.mvvm.ui.produk.adapter.StokProdukAdapter
import com.semout.framework.mvvm.ui.produk.features.produk.AddProdukActivity
import com.semout.framework.mvvm.ui.produk.features.stok.AddCatatanStokActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.ProdukViewModel
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import com.semout.framework.mvvm.utils.ext.EtToString
import org.koin.android.viewmodel.ext.android.viewModel


class StokFragment : BaseFragment() {
    lateinit var binding: FragmentTabStokBinding
    var ibarcode: String = ""
    private val mviewModel by viewModel<ProdukViewModel>()
    var listProduk: ArrayList<ProdukDao.InfoStok?>? = null
    private lateinit var tableView:SortableCarTableView
    private val adapterStok = ListStokAdapter()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabStokBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()

        initData()
        actionUi()
    }

    override fun observeChange() {
    }

    private fun initData() {
        mviewModel.getStok()
        mviewModel.loadProdukStok()
        listProduk = ArrayList()
        var mAdapter =
            object :
                GenericAdapter<ProdukDao.InfoStok?, ItemStokProdukBinding?>(
                    requireContext(),
                    listProduk!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_stok_produk
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String {
                    return "Stok"
                }

                @SuppressLint("SetTextI18n", "ResourceAsColor")
                override fun onBindData(
                    model: ProdukDao.InfoStok?,
                    position: Int,
                    dataBinding: ItemStokProdukBinding?
                ) {
                    val requestOptions = RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                        .skipMemoryCache(true)

                    if (position == 0) {
//                        dataBinding!!.container.setBackgroundColor(R.color.blue_grey_700)
                        dataBinding!!.TvNamaProduk.text = "Nama Produk"
                        dataBinding.TvNamaProduk.setTextColor(R.color.white)
                        dataBinding.k1.text = "K1"
                        dataBinding.k1.setTextColor(R.color.white)
                        dataBinding.k2.text = "K2"
                        dataBinding.k2.setTextColor(R.color.white)
                        dataBinding.rak.text = "Rak"
                        dataBinding.rak.setTextColor(R.color.white)
                        dataBinding.minmax.text = "Min|Max"
                        dataBinding.minmax.setTextColor(R.color.white)
//                        dataBinding.imageViewAvatar.visibility = View.INVISIBLE
//                        dataBinding.checkbox.visibility = View.INVISIBLE
                    } else {
//                        Glide.with(requireContext())
//                            .load(File(model!!.gambar))
//                            .placeholder(R.drawable.image_placeholder)
//                            .apply(requestOptions)
//                            .transition(DrawableTransitionOptions.withCrossFade())
//                            .into(dataBinding!!.imageViewAvatar)

                        if (model!!.stok <= model.min_stok) {
//                            dataBinding.lnTabel.setBackgroundColor(resources.getColor(R.color.red_300))
//                            dataBinding.tvNamaProduk.setTextColor(resources.getColor(R.color.white))
                        }
                        dataBinding!!.TvNamaProduk.text = model.nm_produk
//                    dataBinding.k1.text = model.total_stok.toString()
                        dataBinding.k1.text = model.stok.toString()
                        dataBinding.k2.text = "0"
                        dataBinding.rak.text = model.letak_rak
                        dataBinding.minmax.text =
                            model.min_stok.toString() + "|" + model.max_stok.toString()
                    }
                }

                override fun onItemClick(model: ProdukDao.InfoStok?, position: Int) {
                    startActivity(
                        AddCatatanStokActivity.newIntent(
                            requireContext(),
                            model!!.id_produk
                        )
                    )
                }
            }
        binding.recyclerView.adapter = adapterStok
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        mviewModel.getStokProduk()!!.observe(requireActivity(), {
            val header = ProdukDao.InfoStok()
            header.id_produk = 0
            header.nm_produk = "nama"
            header.gambar = "nama"
            header.min_stok = 0
            header.max_stok = 0
            header.stok = 0
            header.total_stok = 0
            adapterStok.onReplace(it)
        })
    }

    private fun initUi() {
        binding.etSearch.hint = "barcode|produk|kode Rak"
    }

    private fun observeStok(data: List<ProdukDao.InfoStok>){
        adapterStok.onReplace(data)
    }

    private fun actionUi() {
        binding.imvBarcode.setOnClickListener {
            val i = Intent(requireContext(), BarcodeScannerActivity::class.java)
            startActivityForResult(i, 2)
        }
        binding.BtnAddProduk.setOnClickListener {
            val i = Intent(requireContext(), AddProdukActivity::class.java)
            startActivity(i)
        }

        binding.btnCari.setOnClickListener {
            val search = binding.etSearch.EtToString()
            if (search.isNotEmpty()) {
                mviewModel.getSearchStok(search)
            } else {
                binding.etSearch.error = "Isi Pencarian"
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2) {
            val barcode: String? = data?.getStringExtra(BarcodeScannerActivity.BARCODE_TYPE)
            ibarcode = barcode ?: ""
            binding.etSearch.setText(barcode)
        }
    }
}