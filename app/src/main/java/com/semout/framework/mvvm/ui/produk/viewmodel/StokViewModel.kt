package com.semout.framework.mvvm.ui.produk.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.semout.framework.mvvm.core.BaseViewModel
import com.semout.framework.mvvm.data.model.local.db.KategoriProduk
import com.semout.framework.mvvm.data.model.local.db.Kemasan
import com.semout.framework.mvvm.data.model.local.db.Produk
import com.semout.framework.mvvm.data.model.local.db.Satuan
import com.semout.framework.mvvm.data.repository.Repository
import com.semout.framework.mvvm.utils.NetworkHelper
import kotlinx.coroutines.launch

class StokViewModel(
    private val mainRepository: Repository
) : BaseViewModel() {

    val mTrigerProduk: MutableLiveData<Boolean> = MutableLiveData()
    val mTriger: MutableLiveData<Boolean> = MutableLiveData()
    val mTrigerKemasan: MutableLiveData<Boolean> = MutableLiveData()
    val mTrigersatuan: MutableLiveData<Boolean> = MutableLiveData()
    val mTrigerSave: MutableLiveData<Boolean> = MutableLiveData()
    val kategori: LiveData<List<KategoriProduk>> = Transformations.switchMap(mTriger) {
        mainRepository.getKategoriProduk()
    }
    val satuan: LiveData<List<Satuan>> = Transformations.switchMap(mTrigersatuan) {
        mainRepository.getSatuan()
    }
    val kemasan: LiveData<List<Kemasan>> = Transformations.switchMap(mTrigerKemasan) {
        mainRepository.getKemasan()
    }

    @JvmName("getKategori1")
    fun getKategori(): LiveData<List<KategoriProduk>> {
        return kategori
    }

    @JvmName("getSatuan1")
    fun getSatuan(): LiveData<List<Satuan>> {
        return satuan
    }

    @JvmName("getKemasan1")
    fun getKemasan(): LiveData<List<Kemasan>> {
        return kemasan
    }

    val produk: LiveData<List<Produk>> = Transformations.switchMap(mTrigerProduk) {
        mainRepository.getProduk()
    }

    @JvmName("getProduk1")
    fun getProduk(): LiveData<List<Produk>> {
        return produk
    }

    fun loadProduk() {
        mTrigerProduk.value = true
    }

    fun loadSatuan() {
        mTrigersatuan.value = true
    }

    fun loadKategori() {
        mTriger.value = true
    }

    fun loadKemasan() {
        mTrigerKemasan.value = true
    }

    fun addProduk(
        kdbarcode: String,
        nama: String,
        bminstok: Int,
        bmaxstok: Int,
        idKat: Int,
        idSatuan: Int,
        idKemasan: Int,
        berat: Double,
        harga_jual: Double,
        letak_rak: String,
        Pic: String
    ) {
        val model = Produk()
        model.b_max_stok = bmaxstok
        model.b_min_stok = bminstok
        model.idkategori = idKat
        model.gambar = Pic
        model.idsatuan = idSatuan
        model.idkemasan = idKemasan
        model.berat = berat
        model.letak_rak = letak_rak
        model.hg_jual = harga_jual
        model.namaProduk = nama
        model.barcode = kdbarcode
        model.promo = 0
        model.k1 = 0
        model.k2 = 0
        mainRepository.createProduk(model)
        mTrigerSave.postValue(true)
    }
}