package com.semout.framework.mvvm.ui.produk.features.pembelian

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.data.model.local.db.MasterSuplier
import com.semout.framework.mvvm.data.model.local.db.TrxPembelianTemp
import com.semout.framework.mvvm.databinding.ActivityPembelianNewBinding
import com.semout.framework.mvvm.databinding.ItemProdukPembelianBinding
import com.semout.framework.mvvm.ui.produk.viewmodel.PembelianViewModel
import com.semout.framework.mvvm.ui.produk.widget.BottomSheetPembelian
import com.semout.framework.mvvm.utils.KasirHelper
import com.semout.framework.mvvm.utils.NumberUtils
import com.semout.framework.mvvm.utils.ProjectUtils
import com.semout.framework.mvvm.utils.TimeFormat.DATE_FORMAT_DEFAULT
import com.semout.framework.mvvm.utils.TimeFormat.DATE_FORMAT_RECOMMENDATION
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import com.semout.framework.mvvm.utils.ext.EtToString
import com.semout.framework.mvvm.utils.ext.OnTextChangedListener
import com.semout.framework.mvvm.utils.ext.observe
import org.koin.android.viewmodel.ext.android.viewModel

class PembelianActivity : BaseActivity(), BottomSheetPembelian.PembelianListener {
    var ibarcode: String = ""
    lateinit var binding: ActivityPembelianNewBinding
    private val mviewModel by viewModel<PembelianViewModel>()
    var listSuplier: ArrayList<MasterSuplier?>? = null
    var kd_trxBeli: String = ""
    var idSuplier: Int = 0
    var nameSuplier: String = ""
    var tglSuplier: String = ""
    var potonganNota = 0.0
    var totalNota = "0"

    var listTrxBeli: ArrayList<TrxPembelianTemp?>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPembelianNewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initData()
        initUi()
        action()
    }

    private fun initUi() {
        BottomSheetPembelian.showCustomViewDialog(
            this, listSuplier!!,
            BottomSheet(LayoutMode.WRAP_CONTENT), this, tglSuplier, nameSuplier
        )

        val behavior: BottomSheetBehavior<*> =
            BottomSheetBehavior.from<LinearLayout>(binding.bttomLay)
        behavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun initData() {
        listSuplier = ArrayList()
        listTrxBeli = ArrayList()
        kd_trxBeli = KasirHelper.getKodeTrxPembelian()
        mviewModel.setKodeTrxPembelian(kd_trxBeli)
        mviewModel.getLoadSuplier()
        //load list pembelian
        mviewModel.getTrxPembelian(kd_trxBeli)
        mviewModel.getTotalTrxPembelian(kd_trxBeli)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        val mAdapter =
            object :
                GenericAdapter<TrxPembelianTemp?, ItemProdukPembelianBinding?>(
                    this,
                    listTrxBeli!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_produk_pembelian
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String {
                    return "Nota Beli"
                }

                @SuppressLint("SetTextI18n")
                override fun onBindData(
                    model: TrxPembelianTemp?,
                    position: Int,
                    dataBinding: ItemProdukPembelianBinding?
                ) {

                    val lisSpinner: ArrayList<String> = ArrayList()
                    lisSpinner.add("K1")
                    lisSpinner.add("K2")

                    val adapterK: ArrayAdapter<String> = ArrayAdapter(
                        this@PembelianActivity,
                        android.R.layout.simple_spinner_item, lisSpinner
                    )
                    adapterK.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    dataBinding!!.tipeK1.adapter = adapterK
                    val diskon =
                        if (model!!.diskon_nominal > 0) " - (${model.diskon_nominal})" else ""

                    val diskon_persen =
                        if (model.diskon_persen > 0)
                            NumberUtils.presentasePotHarga(
                                model.diskon_persen.toDouble(),
                                model.harga.toDouble()
                            )
                        else 0.0
                    val diskon_persen_info =
                        if (model.diskon_persen > 0)
                            "[${
                                NumberUtils.presentasePotHarga(
                                    model.diskon_persen.toDouble(),
                                    model.harga.toDouble()
                                ).toString().replace(".0", "")
                            }]"
                        else ""
//                    val _nominal=NumberUtils.convertNumber(model.reharga.toString())
                    // hbelibersih = hbeli-persen-nominal
                    val harga_akr =
                        model.total - (model.diskon_nominal.toDouble() + (model.harga - diskon_persen))
                    val total_akr =
                        if (model.diskon_nominal > 0) model.total.toInt() -
                                (model.diskon_nominal.toDouble()
                                    .plus(diskon_persen)) else model.total

                    val diskonAll =
                        if (diskon_persen_info.isEmpty()) "" else if (diskon.isEmpty()) "" else "$diskon_persen_info $diskon"
                    dataBinding.tvNamaProduk.text =
                        "${model.namaBarang}\n${model.qty} * ${
                            model.harga.toString().replace(".0", "")
                        } - $diskonAll \n=${
                            NumberUtils.convertNumber(
                                total_akr.toString().replace(".0", "")
                            )
                        }"

                    val _total = total_akr.toString().replace(".0", "")
                    dataBinding.k2.text = NumberUtils.convertNumber(_total)
                    dataBinding.action.setOnClickListener {
                        showDialogEdit(model.id, model)
                    }
                }

                override fun onItemClick(model: TrxPembelianTemp?, position: Int) {

                }
            }

        binding.recyclerView.adapter = mAdapter

        mviewModel.getTempPembelian().observe(this) {
            listTrxBeli!!.clear()
            listTrxBeli!!.addAll(it!!)
            mAdapter.notifyDataSetChanged()
            mviewModel.getTotalTrxPembelian(kd_trxBeli)
        }

        //deprecated
        mviewModel.getTotalTempPembelian()?.observe(this) {
//            val _total = if (it.isNotEmpty()) it[0].total.toString().replace(".0", "") else "0"
//            totalNota = _total
//            binding.TvTotal.text = NumberUtils.convertNumber(_total)
//            val _potNota = if (binding.EtPotNota.EtToString().isNotEmpty()
//            ) (totalNota.toInt() - binding.EtPotNota.EtToString().toInt()) else totalNota
//            binding.TvTotalPembeliandiskon.text = NumberUtils.convertNumber(_potNota.toString())
//            potonganNota = totalNota.toDouble()
        }
    }

    fun action() {
        binding.EtPotNota.OnTextChangedListener {
            if (it.isNotEmpty()) {
                if (binding.EtPotNota.EtToString().isNotEmpty()) {
                    val subtotal =
                        (totalNota.toInt() - it.toInt())
                    potonganNota = subtotal.toDouble()
                    binding.TvTotalPembeliandiskon.text =
                        "${NumberUtils.convertNumber(subtotal.toString())}"
                } else {
                    binding.TvTotalPembeliandiskon.text = NumberUtils.convertNumber(totalNota)
                }
            } else {
                potonganNota = totalNota.toDouble()
                binding.TvTotalPembeliandiskon.text = NumberUtils.convertNumber(totalNota)
            }
        }

        binding.lnTop.setOnClickListener {
            BottomSheetPembelian.showCustomViewDialog(
                this, listSuplier!!,
                BottomSheet(LayoutMode.WRAP_CONTENT), this, tglSuplier, nameSuplier
            )
        }

        binding.BackBtn.setOnClickListener {
            finish()
        }
        binding.BtnAddProduk.setOnClickListener {
            startActivity(
                AddPembelianActivity.newIntent(
                    this,
                    0,
                    idSuplier,
                    nameSuplier,
                    kd_trxBeli,
                    "add"
                )
            )
        }
        //selesai
        binding.BtnBayar.setOnClickListener {
            MaterialDialog(this).show {
                title(text = "Informasi")
                icon(R.drawable.ic_baseline_warning_24)
                message(text = "Apakah anda yakin untuk menyelesaikan transaksi ini ?")
                positiveButton(R.string.yes) { dialog ->
                    //clear
                    mviewModel.selesai(
                        kd_trxBeli,
                        idSuplier,
                        potonganNota,
                        totalNota.toDouble(),
                        tglSuplier
                    )
                    binding.EtPotNota.setText("")
                    binding.TvTotalPembeliandiskon.text = "0"
                    dialog.dismiss()
                }
                negativeButton(R.string.no) { dialog ->
                    dialog.dismiss()
                    // Do something
                }
            }
        }

    }

    override fun observeChange() {
        observe(mviewModel.suplier, ::loadSpinSuplier)
        observe(mviewModel.totalTrx_pembelian_temp, ::loadTotalTrxPembelianTmp)
    }

    private fun loadTotalTrxPembelianTmp(totalTrxTmpBeli: List<TrxPembelianTemp>) {
        if (!totalTrxTmpBeli.isNullOrEmpty()) {
            val _total = if (totalTrxTmpBeli.isNotEmpty()) totalTrxTmpBeli[0].total.toString()
                .replace(".0", "") else "0"
            totalNota = _total
            binding.TvTotal.text = NumberUtils.convertNumber(_total)
            val _potNota = if (binding.EtPotNota.EtToString().isNotEmpty()
            ) (totalNota.toInt() - binding.EtPotNota.EtToString().toInt()) else totalNota
            binding.TvTotalPembeliandiskon.text = NumberUtils.convertNumber(_potNota.toString())
            potonganNota = totalNota.toDouble()
        }
    }

    override fun onResume() {
        super.onResume()
        if (kd_trxBeli.isNotEmpty()) {
            mviewModel.getTrxPembelian(kd_trxBeli)
            mviewModel.getTotalTrxPembelian(kd_trxBeli)
        }
    }

    private fun loadSpinSuplier(mlistSuplier: List<MasterSuplier>) {
        listSuplier = ArrayList()
        if (mlistSuplier.isNotEmpty()) {
            listSuplier!!.addAll(mlistSuplier)
            val listSpinner: MutableList<String> = ArrayList()
//            listSpinner.add()
            for (i in 0 until listSuplier!!.size) {
                listSpinner.add(listSuplier!![i]!!.nama_suplier)
            }
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item, listSpinner
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//            binding.SpinnerSuplier.adapter = adapter
//            if (listSuplier!!.size > 0)
//                BottomSheetPembelian.showCustomViewDialog(
//                    this, listSuplier!!,
//                    BottomSheet(LayoutMode.WRAP_CONTENT), this, tglSuplier, nameSuplier
//                )
        }
    }

    fun showDialogEdit(idItem: Int, model: TrxPembelianTemp) {
//        MaterialDialog(this).show {
//            message(text = "Apakah anda yakin ")
//            positiveButton(R.string.edit) { dialog ->
//                showMessageToast(idItem.toString())
//                startActivity(
//                    AddPembelianActivity.newIntent(
//                        this@PembelianActivity,
//                        idItem,
//                        idSuplier,
//                        nameSuplier,
//                        kd_trxBeli,
//                        "edit"
//                    )
//                )
//                dialog.dismiss()
//                // Do something
//            }
//            negativeButton(R.string.hapus) { dialog ->
        mviewModel.hapusPembelian(idItem)
//                dialog.dismiss()
        // Do something
//            }
//        }
    }

    override fun setPembelianTrx(tgl: String, idSuplier: Int, nmSuplier: String) {
        this.idSuplier = idSuplier
        tglSuplier = tgl
        nameSuplier = nmSuplier
        val inDateBirth =
            if (tgl.isNotEmpty()) ProjectUtils.formatDatetoOtherPattern(
                tgl,
                DATE_FORMAT_DEFAULT,
                DATE_FORMAT_RECOMMENDATION
            ) else ""
        binding.suplier.text = nmSuplier
        binding.TvTglTrx.text = inDateBirth
    }

    var x = 0;
    override fun onBackPressed() {
        if (x == 0) {
            showMessageToast("Tekan sekali lagi")
            x += 1
        }
        if (x == 1) {
            MaterialDialog(this).show {
                title(text = "Informasi")
                icon(R.drawable.ic_baseline_warning_24)
                message(text = "Apakah anda yakin untuk keluar transaksi ? \n Transaksi sebelumnya akan di hapus ")
                positiveButton(R.string.yes) { dialog ->
                    //clear
                    dialog.dismiss()
                    finish()
                    // Do something
                }
                negativeButton(R.string.no) { dialog ->
                    dialog.dismiss()
                    // Do something
                }
            }
        }
    }

}