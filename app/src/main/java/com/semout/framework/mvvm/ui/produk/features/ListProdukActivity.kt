package com.semout.framework.mvvm.ui.produk.features

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.data.model.local.db.Produk
import com.semout.framework.mvvm.databinding.ItemProdukBinding
import com.semout.framework.mvvm.databinding.ProdukListScreenBinding
import com.semout.framework.mvvm.ui.produk.features.pembelian.AddPembelianActivity
import com.semout.framework.mvvm.ui.produk.features.pembelian.AddPembelianActivity.Companion.EXTRA_ID_PRODUK
import com.semout.framework.mvvm.ui.produk.features.pembelian.AddPembelianActivity.Companion.EXTRA_ISI_K2
import com.semout.framework.mvvm.ui.produk.features.pembelian.AddPembelianActivity.Companion.EXTRA_KP
import com.semout.framework.mvvm.ui.produk.features.pembelian.AddPembelianActivity.Companion.EXTRA_NAMA_PRODUK
import com.semout.framework.mvvm.ui.produk.features.pembelian.AddPembelianActivity.Companion.EXTRA_REQ_ID_PRODUK
import com.semout.framework.mvvm.ui.produk.viewmodel.ProdukViewModel
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import com.semout.framework.mvvm.utils.ext.EtToString
import com.semout.framework.mvvm.utils.ext.observe
import kotlinx.android.synthetic.main.produkstok_main_screen.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File

class ListProdukActivity : BaseActivity() {
    lateinit var binding: ProdukListScreenBinding
    var listProduk: ArrayList<Produk?>? = null
    private val mviewModel by viewModel<ProdukViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ProdukListScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUi()
        initData()
        action()
    }

    private fun initUi() {
        binding.textLabelToolbar.text="List Produk"

    }

    private fun initData() {
        mviewModel.getProduk()
        mviewModel.loadProduk()
        listProduk = ArrayList()
        var mAdapter =
            object :
                GenericAdapter<Produk?, ItemProdukBinding?>(
                    this@ListProdukActivity,
                    listProduk!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_produk
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String? {
                    return "Produk"
                }

                @SuppressLint("SetTextI18n")
                override fun onBindData(
                    model: Produk?,
                    position: Int,
                    dataBinding: ItemProdukBinding?
                ) {
                    val requestOptions = RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                        .skipMemoryCache(true)

                    Glide.with(this@ListProdukActivity)
                        .load(File(model!!.gambar))
                        .placeholder(R.drawable.image_placeholder)
                        .apply(requestOptions)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(dataBinding!!.imageViewAvatar)

                    dataBinding.textViewUserName.text = model.namaProduk
                    dataBinding.textViewUserEmail.text =
                        "Rp.${model.hg_jual.toString().replace(".0", "")}"
                }

                override fun onItemClick(model: Produk?, position: Int) {
                    val intent = Intent()
                    intent.putExtra(EXTRA_ID_PRODUK, model!!.id)
                    intent.putExtra(EXTRA_NAMA_PRODUK, model.namaProduk)
                    intent.putExtra(EXTRA_ISI_K2, model.k2)
                    intent.putExtra(EXTRA_KP, model.idkemasan)
                    setResult(EXTRA_REQ_ID_PRODUK, intent)
                    finish() //finishing activity
                }


            }
        binding.recyclerViewProduk.adapter = mAdapter
        binding.recyclerViewProduk.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mviewModel.getProduk().observe(this) {
            listProduk!!.clear()
            listProduk!!.addAll(it!!)
            mAdapter.notifyDataSetChanged()
        }

        mviewModel.getProdukSearch()?.observe(this) {
            listProduk!!.clear()
            listProduk!!.addAll(it!!)
            mAdapter.notifyDataSetChanged()
        }
    }

    fun action() {
        binding.BtnCari.setOnClickListener {
            if (binding.EtSearchProduk.EtToString().isNotEmpty()){
                mviewModel.getProdukSearch(binding.EtSearchProduk.EtToString())
            }
        }
    }

    override fun observeChange() {
    }


    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, ListProdukActivity::class.java)
            return intent
        }
    }

}