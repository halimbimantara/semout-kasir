package com.semout.framework.mvvm.data.local.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.semout.framework.mvvm.data.model.api.User;
import com.semout.framework.mvvm.data.model.local.db.KategoriProduk;
import com.semout.framework.mvvm.data.model.local.db.Kemasan;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface KemasanProdukDao {

    @Delete
    void delete(KategoriProduk user);

    @Query("SELECT * FROM kemasan WHERE nama LIKE :name LIMIT 1")
    Single<Kemasan> findByName(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Kemasan kemasan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Kemasan> kemasans);

    @Query("SELECT * FROM kemasan")
    Flowable<List<Kemasan>> loadAll();

    @Query("SELECT * FROM kemasan WHERE id IN (:kemasanIds)")
    Flowable<List<Kemasan>> loadAllById(List<Integer> kemasanIds);
}
