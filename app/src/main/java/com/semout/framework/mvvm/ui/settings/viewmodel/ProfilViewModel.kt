package com.semout.framework.mvvm.ui.settings.viewmodel

import com.semout.framework.mvvm.BuildConfig
import com.semout.framework.mvvm.core.BaseViewModel
import com.semout.framework.mvvm.data.pref.AppPrefSource
import com.semout.framework.mvvm.data.repository.Repository

class ProfilViewModel(
    private val mainRepository: Repository,
    private val pref: AppPrefSource,
) : BaseViewModel() {
    val PicObservable = "${BuildConfig.BASE_URL}resources/uploads/logo/${pref.getPhoto()}"
    val usernameObservable = pref.getFullName()!!
    val usermailObservable = pref.getEmail()
}