package com.semout.framework.mvvm.ui.produk.fragment

import android.R
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.data.model.local.db.MasterSuplier
import com.semout.framework.mvvm.databinding.FragmentTabSuplierBinding
import com.semout.framework.mvvm.ui.produk.features.produk.AddProdukActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.PembelianViewModel
import com.semout.framework.mvvm.utils.ext.observe
import org.koin.android.viewmodel.ext.android.viewModel

class SuplierFragment : BaseFragment() {
    var listSuplier: ArrayList<MasterSuplier?>? = null
    lateinit var binding: FragmentTabSuplierBinding
    private val mviewModel by viewModel<PembelianViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTabSuplierBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        actionUi()
    }

    override fun observeChange() {
        observe(mviewModel.suplier, ::loadSpinSuplier)
    }

    private fun initData() {
        mviewModel.getSuplier()
        mviewModel.loadSuplier()
    }

    private fun loadSpinSuplier(mlistSuplier: List<MasterSuplier>) {
        listSuplier = ArrayList()
        if (mlistSuplier.isNotEmpty()) {
            listSuplier!!.addAll(mlistSuplier)
            val listSpinner: MutableList<String> = ArrayList()
            for (i in 0 until listSuplier!!.size) {
                listSpinner.add(listSuplier!![i]!!.nama_suplier)
            }
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                requireContext(),
                R.layout.simple_spinner_item, listSpinner
            )
            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
            binding.SpinnerSuplier.adapter = adapter
        }
    }

    private fun actionUi() {
        binding.BtnAddProduk.setOnClickListener {
            val i = Intent(requireContext(), AddProdukActivity::class.java)
            startActivity(i)
        }
    }
}