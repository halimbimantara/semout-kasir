package com.semout.framework.mvvm.ui.main.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.ui.main.model.SliderItem
import com.smarteist.autoimageslider.SliderViewAdapter
import kotlinx.android.synthetic.main.image_slider_layout_item.view.*


class SliderAdapter(
    private val context: Context
) : SliderViewAdapter<SliderAdapter.SliderAdapterVH>() {
    private val slide: ArrayList<SliderItem> = ArrayList()
    class SliderAdapterVH(itemView: View?) : SliderViewAdapter.ViewHolder(itemView) {
        var imageViewBackground: ImageView? = itemView!!.iv_auto_image_slider
        var textViewDescription: TextView? = itemView!!.tv_auto_image_slider
    }

    fun addData(list: List<SliderItem>) {
        slide.addAll(list)
    }

    fun deleteItem(position: Int) {
        slide.removeAt(position)
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return slide.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?): SliderAdapterVH {
        val inflate: View = LayoutInflater.from(parent!!.context)
            .inflate(R.layout.image_slider_layout_item, null)
        return SliderAdapterVH(inflate)
    }

    override fun onBindViewHolder(itemView: SliderAdapterVH?, position: Int) {
            itemView?.textViewDescription!!.text = slide[position].description
            itemView.textViewDescription!!.textSize = 16f
            itemView.textViewDescription!!.setTextColor(Color.WHITE)
            itemView.imageViewBackground?.let {
                Glide.with(context)
                    .load(slide[position].imageDrawable)
                    .fitCenter()
                    .into(it)
            }

    }
}