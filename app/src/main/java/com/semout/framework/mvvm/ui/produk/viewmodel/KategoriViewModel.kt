package com.semout.framework.mvvm.ui.produk.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.semout.framework.mvvm.core.BaseViewModel
import com.semout.framework.mvvm.data.model.local.db.KategoriProduk
import com.semout.framework.mvvm.data.repository.AppRepository
import com.semout.framework.mvvm.data.repository.Repository
import com.semout.framework.mvvm.utils.NetworkHelper
import kotlinx.coroutines.launch

class KategoriViewModel(
    private val mainRepository: Repository
) : BaseViewModel() {

    val mTriger: MutableLiveData<Boolean> = MutableLiveData()

    val kategori: LiveData<List<KategoriProduk>> = Transformations.switchMap(mTriger) {
        mainRepository.getKategoriProduk()
    }

    @JvmName("getKategori1")
    fun getKategori(): LiveData<List<KategoriProduk>> {
        return kategori
    }

    fun loadKategori() {
        mTriger.value = true
    }

    fun addKategori(kategori: String) {
        val model = KategoriProduk()
        model.nama_kategori = kategori
        model.parent = 0
        mainRepository.createKategoriProduk(model)
        mTriger.postValue(true)
    }

}