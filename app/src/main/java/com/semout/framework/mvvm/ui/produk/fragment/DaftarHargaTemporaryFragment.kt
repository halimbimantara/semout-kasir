package com.semout.framework.mvvm.ui.produk.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.data.model.local.db.TrxSearchTemporary
import com.semout.framework.mvvm.databinding.FragmentTabProdukBinding
import com.semout.framework.mvvm.databinding.ItemProdukBinding
import com.semout.framework.mvvm.ui.kasir.BarcodeScannerActivity
import com.semout.framework.mvvm.ui.produk.features.produk.AddProdukActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.ProdukViewModel
import com.semout.framework.mvvm.utils.NumberUtils
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import com.semout.framework.mvvm.utils.ext.observe
import org.koin.android.viewmodel.ext.android.viewModel


class DaftarHargaTemporaryFragment : BaseFragment() {
    private val page_no = 1
    private val no_of_records_per_page = 10

    private var mAdapter: GenericAdapter<TrxSearchTemporary?, ItemProdukBinding?>? = null

    private val mviewModel by viewModel<ProdukViewModel>()
    lateinit var binding: FragmentTabProdukBinding
    var ibarcode: String = ""
    var listProduk: ArrayList<TrxSearchTemporary?>? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabProdukBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionUi()
        initUi()
        initData()
    }

    private fun initData() {
        mviewModel.getProdukSearchV2(page_no, no_of_records_per_page)
        listProduk = ArrayList()
        mAdapter =
            object : GenericAdapter<TrxSearchTemporary?, ItemProdukBinding?>(
                requireContext(),
                listProduk!!
            ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_produk
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String? {
                    return "Produk"
                }

                @SuppressLint("SetTextI18n")
                override fun onBindData(
                    model: TrxSearchTemporary?,
                    position: Int,
                    dataBinding: ItemProdukBinding?
                ) {
                    val requestOptions = RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                        .skipMemoryCache(true)


                    dataBinding!!.textViewUserName.text = model?.nama_produk
                    dataBinding.textViewUserEmail.text =
                        "Rp.${
                            NumberUtils.convertNumber(
                                model!!.harga.toString().replace(".0", "")
                            )
                        }"
                    dataBinding.imvOptionmenu.setOnClickListener {
                        openOptionMenu(it,position,model.id,model.nama_produk,model.harga)
                    }
                }

                override fun onItemClick(model: TrxSearchTemporary?, position: Int) {
                }
            }
        binding.recyclerView.adapter = mAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL,
            false
        )

    }

    fun openOptionMenu(v: View, position: Int,id:Int,nm:String,harga:Double) {
        val popup = PopupMenu(v.context, v)
        popup.menuInflater.inflate(R.menu.produk_navigation, popup.menu)
        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.act_add -> {
                    startActivity(AddProdukActivity.newIntentAddDaftarHarga(requireContext(),2,nm,harga.toString(),id))
                }
                R.id.act_del -> {
                    print("ok")
                }
                else ->
                    print("ss")
            }
             true
        }
        popup.show()
    }

    private fun initUi() {

    }

    override fun observeChange() {
        observe(mviewModel.getProdukTemporary, ::loadProdukTemp)
    }

    private fun loadProdukTemp(data: List<TrxSearchTemporary>) {
        listProduk!!.addAll(data)
        mAdapter!!.notifyDataSetChanged()
    }


    private fun actionUi() {
        binding.BtnAddProduk.setOnClickListener {
            val i = Intent(requireContext(), AddProdukActivity::class.java)
            startActivity(i)
        }

        binding.imvBarcode.setOnClickListener {
            val x = Intent(requireContext(), BarcodeScannerActivity::class.java)
            startActivityForResult(x, 2)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === 2) {
            val barcode: String? = data?.getStringExtra(BarcodeScannerActivity.BARCODE_TYPE)
            ibarcode = barcode ?: ""
            binding.etSearch.setText(barcode)
        }
    }
}