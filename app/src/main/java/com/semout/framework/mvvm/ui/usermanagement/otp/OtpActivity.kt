package com.semout.framework.mvvm.ui.usermanagement.otp

import android.content.Intent
import android.os.Bundle
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.ui.usermanagement.login.LoginViewModel
import com.semout.framework.mvvm.ui.usermanagement.register.activity.RegisterActivity
import kotlinx.android.synthetic.main.otp_screen.*
import org.koin.android.viewmodel.ext.android.viewModel

class OtpActivity : BaseActivity() {
    private val loginViewModel by viewModel<LoginViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.otp_screen)
        observeChange()
        BtnMasuk.setOnClickListener({
            val intenr = Intent(this@OtpActivity, RegisterActivity::class.java)
            startActivity(intenr)
        })
    }

    override fun observeChange() {
    }
}