package com.semout.framework.mvvm.ui.kasir.fragment

import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.data.model.local.db.KategoriProduk
import com.semout.framework.mvvm.databinding.FragmentTabKemasanBinding
import com.semout.framework.mvvm.databinding.ItemKategoriBinding
import com.semout.framework.mvvm.databinding.ItemKategoriIconBinding
import com.semout.framework.mvvm.ui.produk.features.AddKategoriProdukActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.KategoriViewModel
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import com.semout.framework.mvvm.utils.ext.gone
import org.koin.android.viewmodel.ext.android.viewModel
import kotlin.math.roundToInt

class KasirKategoriFragment : DialogFragment() {
    private val mviewModel by viewModel<KategoriViewModel>()
    lateinit var binding: FragmentTabKemasanBinding
    var listKategori: ArrayList<KategoriProduk?>? = null

    companion object {
        const val TAG = "KasirKategoriFragment"
        fun newInstance(): KasirKategoriFragment {
            val args = Bundle()
            val fragment = KasirKategoriFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabKemasanBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionUi()
        initUi()
        initData()
    }

    private fun initData() {
        mviewModel.getKategori()
        mviewModel.loadKategori()
        listKategori = ArrayList()
        var mAdapter =
            object :
                GenericAdapter<KategoriProduk?, ItemKategoriIconBinding?>(
                    requireContext(),
                    listKategori!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_kategori_icon
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String? {
                    return "Kategori"
                }

                override fun onBindData(
                    model: KategoriProduk?,
                    position: Int,
                    dataBinding: ItemKategoriIconBinding?
                ) {
                    dataBinding!!.textViewName.setText(model!!.nama_kategori)
                }

                override fun onItemClick(model: KategoriProduk?, position: Int) {

                }


            }
        val display = requireActivity().windowManager.defaultDisplay
        val outMetrics = DisplayMetrics()
        display.getMetrics(outMetrics)
        val density = resources.displayMetrics.density
        val dpWidth: Float = outMetrics.widthPixels / density
        val columns = (dpWidth / 130).roundToInt()

        binding.recyclerView.adapter = mAdapter
        binding.recyclerView.layoutManager =
            GridLayoutManager(requireContext(), columns)
        mviewModel.getKategori().observe(requireActivity(), {
            listKategori!!.clear()
            listKategori!!.addAll(it!!)
            mAdapter.notifyDataSetChanged()
        })

    }

    private fun initUi() {
        binding.BtnAddProduk.gone()
    }

    private fun actionUi() {
        binding.BtnAddProduk.setOnClickListener {
            val i = Intent(requireContext(), AddKategoriProdukActivity::class.java)
            startActivity(i)
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
    }
}