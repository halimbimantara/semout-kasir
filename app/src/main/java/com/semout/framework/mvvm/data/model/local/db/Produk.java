/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.semout.framework.mvvm.data.model.local.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "produk")
public class Produk {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "nama")
    public String namaProduk;

    @ColumnInfo(name = "kd_barcode")
    public String barcode;

    @ColumnInfo(name = "id_kategori")
    public int idkategori;

    @ColumnInfo(name = "id_kemasan")
    public int idkemasan;

    @ColumnInfo(name = "berat")
    public double berat;

    @ColumnInfo(name = "id_satuan")
    public int idsatuan;

    /**
     * k1 ,k2,kp adalah tipe kemasan
     */
    @ColumnInfo(name = "k1")
    public int k1;

    @ColumnInfo(name = "k2")
    public int k2;

    @ColumnInfo(name = "kp")
    public int kp;

    @ColumnInfo(name = "promo")
    public int promo;

    @ColumnInfo(name = "gambar")
    public String gambar;

    @ColumnInfo(name = "harga_dasar")
    public double hg_dasar;

    @ColumnInfo(name = "harga_jual")
    public double hg_jual;

    @ColumnInfo(name = "rak")
    public String letak_rak;

    @ColumnInfo(name = "batas_min_stok")
    public int b_min_stok;

    @ColumnInfo(name = "batas_max_stok")
    public int b_max_stok;

}
