package com.semout.framework.mvvm.ui.produk.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.semout.framework.mvvm.data.local.dao.ProdukDao.InfoStok
import de.codecrafters.tableview.TableView
import de.codecrafters.tableview.toolkit.LongPressAwareTableDataAdapter
import java.text.NumberFormat

class StokTableDataAdapter(
    context: Context?,
    data: List<InfoStok?>?,
    tableView: TableView<InfoStok?>?
) : LongPressAwareTableDataAdapter<InfoStok?>(context, data, tableView) {
    override fun getDefaultCellView(rowIndex: Int, columnIndex: Int, parentView: ViewGroup): View {
        val stok = getRowData(rowIndex)
        var renderedView: View? = null
        when (columnIndex) {
            0 -> renderedView = renderString(stok!!.nm_produk)
            1 -> renderedView = renderString(stok!!.stok.toString())
            2 -> renderedView = renderString(stok!!.max_stok.toString())
            3 -> renderedView = renderString(stok!!.min_stok.toString())
            4 -> renderedView = renderString(stok!!.letak_rak)
        }
        return renderedView!!
    }

    override fun getLongPressCellView(
        rowIndex: Int,
        columnIndex: Int,
        parentView: ViewGroup
    ): View {
        val car = getRowData(rowIndex)
        var renderedView: View? = null
        when (columnIndex) {
            1 -> {
            }
            else -> renderedView = getDefaultCellView(rowIndex, columnIndex, parentView)
        }
        return renderedView!!
    }

    //    private View renderEditableCatName(final Car car) {
    //        final EditText editText = new EditText(getContext());
    //        editText.setText(car.getName());
    //        editText.setPadding(20, 10, 20, 10);
    //        editText.setTextSize(TEXT_SIZE);
    //        editText.setSingleLine();
    //        editText.addTextChangedListener(new CarNameUpdater(car));
    //        return editText;
    //    }
    //
    //    private View renderPrice(final Car car) {
    //        final String priceString = PRICE_FORMATTER.format(car.getPrice()) + " €";
    //
    //        final TextView textView = new TextView(getContext());
    //        textView.setText(priceString);
    //        textView.setPadding(20, 10, 20, 10);
    //        textView.setTextSize(TEXT_SIZE);
    //
    //        if (car.getPrice() < 50000) {
    //            textView.setTextColor(ContextCompat.getColor(getContext(), R.color.table_price_low));
    //        } else if (car.getPrice() > 100000) {
    //            textView.setTextColor(ContextCompat.getColor(getContext(), R.color.table_price_high));
    //        }
    //
    //        return textView;
    //    }
    //
    //    private View renderPower(final ProdukDao.InfoStok car, final ViewGroup parentView) {
    //        final View view = getLayoutInflater().inflate(R.layout.table_cell_power, parentView, false);
    //        final TextView kwView = (TextView) view.findViewById(R.id.kw_view);
    //        final TextView psView = (TextView) view.findViewById(R.id.ps_view);
    //
    //        kwView.setText(format(Locale.ENGLISH, "%d %s", car.getKw(), getContext().getString(R.string.kw)));
    //        psView.setText(format(Locale.ENGLISH, "%d %s", car.getPs(), getContext().getString(R.string.ps)));
    //
    //        return view;
    //    }
    //
    //    private View renderCatName(final ProdukDao.InfoStok car) {
    //        return renderString(car.getName());
    //    }
    //
    //    private View renderProducerLogo(final Car car, final ViewGroup parentView) {
    //        final View view = getLayoutInflater().inflate(R.layout.table_cell_image, parentView, false);
    //        final ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
    //        imageView.setImageResource(car.getProducer().getLogo());
    //        return view;
    //    }
    private fun renderString(value: String): View {
        val textView = TextView(context)
        textView.text = value
        textView.setPadding(20, 10, 20, 10)
        textView.textSize = TEXT_SIZE.toFloat()
        return textView
    }

    private class CarNameUpdater(private val carToUpdate: InfoStok) : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            // no used
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            // not used
        }

        override fun afterTextChanged(s: Editable) {
//            carToUpdate.setName(s.toString());
        }
    }

    companion object {
        private const val TEXT_SIZE = 14
        private val PRICE_FORMATTER = NumberFormat.getNumberInstance()
    }
}