package com.semout.framework.mvvm.ui.usermanagement.login.activity

import android.content.Intent
import android.os.Bundle
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.databinding.LoginScreenBinding
import com.semout.framework.mvvm.ui.main.view.MainMenuActivity
import com.semout.framework.mvvm.ui.usermanagement.login.LoginViewModel
import com.semout.framework.mvvm.ui.usermanagement.register.activity.RegisterActivity
import com.semout.framework.mvvm.utils.Utils
import com.semout.framework.mvvm.utils.ext.observe
import kotlinx.android.synthetic.main.login_screen.*
import org.koin.android.viewmodel.ext.android.viewModel

class LoginActivity : BaseActivity() {
    lateinit var binding: LoginScreenBinding
    private val loginViewModel by viewModel<LoginViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LoginScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        action()
        observeChange()
    }

    fun action() {
        binding.BtnLanjutkan.setOnClickListener {
            validasi()
        }
        binding.BtnRegister.setOnClickListener {
            startActivity(RegisterActivity.newIntent(this@LoginActivity))
        }
    }

    fun validasi() {
        val idevice = Utils.getImeiDevice()
        if (EtNoHp.text.toString().isEmpty()) {
            EtNoHp.error = "Harap isi no"
        } else if (EtPassword.text.toString().isEmpty()) {
            EtPassword.error = "Harap isi password"
        } else {
            loginViewModel.login(EtNoHp.text.toString(), EtPassword.text.toString(), idevice)
        }
    }

    override fun observeChange() {
        observe(loginViewModel.isLoged, ::successLogin)
        observe(loginViewModel.errorMessage, ::showSnackbarError)
    }

    private fun successLogin(any: Boolean) {
        if (any) {
            val i = Intent(this@LoginActivity, MainMenuActivity::class.java)
            startActivity(i)
        }
    }
}