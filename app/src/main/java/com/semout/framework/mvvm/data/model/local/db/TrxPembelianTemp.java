/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.semout.framework.mvvm.data.model.local.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "trx_pembelian_temp")
public class TrxPembelianTemp {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "kd_trx_pembelian")
    public String kd_trx_pembelian;

    @ColumnInfo(name = "kd_produk")
    public String kdProduk;

    @ColumnInfo(name = "nama_barang")
    public String namaBarang;

    @ColumnInfo(name = "harga")
    public float harga;

    @ColumnInfo(name = "qty")
    public int qty;

    @ColumnInfo(name = "diskon_persen")
    public int diskon_persen;

    @ColumnInfo(name = "diskon_nominal")
    public int diskon_nominal;

    @ColumnInfo(name = "total_diskon")
    public double tot_diskon;

    @ColumnInfo(name = "stok")
    public int stok;

    @ColumnInfo(name = "kd_satuan")
    public int kd_satuan;

    @ColumnInfo(name = "total")
    public float total;

    @ColumnInfo(name = "keterangan")
    public String keterangan;

}
