package com.semout.framework.mvvm.ui.kasir

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.annotation.NonNull
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.dantsu.escposprinter.connection.DeviceConnection
import com.dantsu.escposprinter.connection.bluetooth.BluetoothConnection
import com.dantsu.escposprinter.connection.bluetooth.BluetoothPrintersConnections
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.otaliastudios.autocomplete.Autocomplete
import com.otaliastudios.autocomplete.AutocompleteCallback
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.data.local.dao.ProdukDao.InfoStokJual
import com.semout.framework.mvvm.data.local.dao.TrxPenjualanTempDao
import com.semout.framework.mvvm.data.local.dao.TrxSearchTemp
import com.semout.framework.mvvm.data.model.local.db.TrxPembelian
import com.semout.framework.mvvm.data.model.local.db.TrxPenjualanTemp
import com.semout.framework.mvvm.data.model.local.db.TrxSearchTemporary
import com.semout.framework.mvvm.databinding.ItemProdukPembelianBinding
import com.semout.framework.mvvm.databinding.MainKasirScreenBinding
import com.semout.framework.mvvm.ui.kasir.helper.AsyncBluetoothEscPosPrint
import com.semout.framework.mvvm.ui.kasir.helper.AsyncEscPosPrinter
import com.semout.framework.mvvm.ui.kasir.viewmodels.KasirViewModel
import com.semout.framework.mvvm.utils.KasirHelper
import com.semout.framework.mvvm.utils.NumberUtils
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import com.semout.framework.mvvm.utils.ext.EtToString
import com.semout.framework.mvvm.utils.ext.OnTextChangedListener
import com.semout.framework.mvvm.utils.ext.observe
import com.semout.framework.mvvm.utils.ext.replaceDecimalComa
import com.skydoves.balloon.ArrowOrientation
import com.skydoves.balloon.Balloon
import com.skydoves.balloon.BalloonAnimation
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.bottom_kasir_screen.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


class KasirMainActivity : BaseActivity(), AutoCompleteProdukPresenter.SearchProdukListener {
    lateinit var binding: MainKasirScreenBinding
    var searchTempProd: Autocomplete<TrxSearchTemp.TrxSearchAll?>? = null
    var listProdukTemp: ArrayList<TrxSearchTemp.TrxSearchAll>? = null
    var listProdukadd: ArrayList<InfoStokJual>? = null
    private val mviewModel by viewModel<KasirViewModel>()
    private var idProduk: Int = 0
    var potonganNota = 0.0

    //harga from temp autocomplete
    private var hargaProduk: Double = 0.0
    private var kode_trx: String = ""
    private var mode_input: String = ""
    var totalNota = "0"
    var totalPotNota = "0"//after diskon

    //temp new item
    private var inProdTempNamaProduk: String = ""
    private var inProdTempHargaProduk: Double = 0.0
    lateinit var presenter: AutoCompleteProdukPresenter
    val INPUT_TIPE_BARCODE = "barcode"
    val INPUT_TIPE_SEARCH = "search"
    val INPUT_TIPE_SEARCH_TEMP = "search_temp"

    var listTrxJual: ArrayList<TrxPenjualanTemp?>? = null
    var inQty: Int = 0;
    var tipeTrx: Int = 0;

    companion object {
        val PERMISSION_BLUETOOTH: Int = 1
    }

    private var mAdapter: GenericAdapter<TrxPenjualanTemp?, ItemProdukPembelianBinding?>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainKasirScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUi()
        initData()
        action()
    }

    private fun initUi() {
        listTrxJual = ArrayList()
        listProdukadd = ArrayList()
        listProdukTemp = ArrayList()

        val behavior: BottomSheetBehavior<*> =
            BottomSheetBehavior.from(binding.inBottom!!.bottomKasir)
        behavior.state = BottomSheetBehavior.STATE_COLLAPSED

        presenter =
            AutoCompleteProdukPresenter(
                this,
                listProdukTemp, this
            )

        val callback: AutocompleteCallback<TrxSearchTemp.TrxSearchAll> =
            object : AutocompleteCallback<TrxSearchTemp.TrxSearchAll> {
                override fun onPopupItemClicked(
                    @NonNull editable: Editable,
                    @NonNull item: TrxSearchTemp.TrxSearchAll
                ): Boolean {
                    editable.clear()
                    editable.append(
                        item.nama_produk + "=${
                            item.harga.toString().replaceDecimalComa()
                        }"
                    )
                    return true
                }

                override fun onPopupVisibilityChanged(shown: Boolean) {}
            }
        val elevation = 6f
        searchTempProd =
            Autocomplete.on<TrxSearchTemp.TrxSearchAll>(binding.inBottom?.EtSearchProduk)
                .with(elevation)
                .with(presenter)
                .with(callback)
                .build()
    }

    private fun initData() {
        kode_trx = KasirHelper.getKodeTrxPenjualan()
        mviewModel.getListTrxPenjualanTmp(kode_trx)
        mviewModel.loadTrxJualTmp()
        mviewModel.mTrigerSavetoTrxJual.observe(this) {
            if (it) {
                Log.i("init", "succes save : ")
                mviewModel.getTotalTrxTempV2(kode_trx)
            }
        }
        getListTrx()
        mviewModel.getListTemporary.observe(this, {
            if (listTrxJual!!.size > 0)
                listTrxJual!!.clear()
            listTrxJual!!.addAll(it!!)
            mAdapter!!.notifyDataSetChanged()
        })

        mviewModel.getListProdukItem().observe(this, {
            if (it.isNotEmpty()) {
                addProdukTemp(it[0].nm_produk, it[0].id_produk, it[0].harga_jual.toDouble())
            }
        })
    }


    private fun getListTrx() {
        mAdapter =
            object :
                GenericAdapter<TrxPenjualanTemp?, ItemProdukPembelianBinding?>(
                    this,
                    listTrxJual!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_produk_pembelian
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String {
                    return "Nota Jual"
                }

                @SuppressLint("SetTextI18n")
                override fun onBindData(
                    model: TrxPenjualanTemp?,
                    position: Int,
                    dataBinding: ItemProdukPembelianBinding?
                ) {
//                    val diskon =
//                        if (model!!.diskon_nominal > 0) " - (${model.diskon_nominal})" else ""
//
//                    val diskon_persen =
//                        if (model.diskon_persen > 0)
//                            NumberUtils.presentasePotHarga(
//                                model.diskon_persen.toDouble(),
//                                model.harga.toDouble()
//                            )
//                        else 0.0
//                    val diskon_persen_info =
//                        if (model.diskon_persen > 0)
//                            NumberUtils.presentasePotHarga(
//                                model.diskon_persen.toDouble(),
//                                model.harga.toDouble()
//                            ).toString().replace(".0", "")
//                        else ""
////                    val _nominal=NumberUtils.convertNumber(model.harga.toString())
                    dataBinding!!.tvNamaProduk.text =
                        "${model!!.namaBarang}\n${model.qty} * ${
                            model.harga.toString().replace(".0", "")
                        } "
                    val total_akr =
                        if (model.diskon > 0) model.total.toInt() - model.diskon else model.total.toInt()
                    val _total = total_akr.toString().replace(".0", "")
                    dataBinding.k2.text = NumberUtils.convertNumber(_total)
//                        NumberUtils.convertNumber(total_akr.toString())?.replace(".0", "")
                    dataBinding.action.setOnClickListener {
                        DeleteItem(model.id, model)
                    }
                }

                override fun onItemClick(model: TrxPenjualanTemp?, position: Int) {

                }
            }
        binding.content!!.rvListProduk.adapter = mAdapter
        binding.content!!.rvListProduk.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    private fun DeleteItem(id: Int, model: TrxPenjualanTemp) {
        mviewModel.hapusProductJualTemp(id)
        Observable.just(true).delay(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map<Any> { o: Boolean? ->
                mviewModel.getListTrxPenjualanTmp(kode_trx)
                mviewModel.getTotalTrxTempV2(kode_trx)
            }
            .subscribe()
    }

    @SuppressLint("SetTextI18n")
    fun action() {

        val balloon = Balloon.Builder(this)
            .setLayout(R.layout.kasir_menu_input)
            .setArrowSize(10)
            .setArrowOrientation(ArrowOrientation.BOTTOM)
            .setArrowPosition(0.5f)
            .setWidthRatio(0.55f)
            .setHeight(135)
            .setCornerRadius(4f)
            .setBackgroundColor(ContextCompat.getColor(this, R.color.black))
            .setBalloonAnimation(BalloonAnimation.FADE)
            .setLifecycleOwner(this)
            .build()

        val btnIcon: ConstraintLayout = balloon.getContentView().findViewById(R.id.menu_icon)
        btnIcon.setOnClickListener {
            balloon.dismiss()
            startActivity(KasirIconActivity.newIntent(this))
        }

        val btnBarcode: ConstraintLayout = balloon.getContentView().findViewById(R.id.menu_barcode)
        btnBarcode.setOnClickListener {
            mode_input = INPUT_TIPE_BARCODE
            val i = Intent(this, BarcodeScannerActivity::class.java)
            startActivityForResult(i, 2)
            balloon.dismiss()
        }

        binding.inBottom!!.BtnInputType.BtnInputType.setOnClickListener {
            balloon.showAlignTop(it)
        }
        binding.inBottom?.BtnBayar!!.setOnClickListener {
            //show preview
            //loading
            mviewModel.selesaiTrx(kode_trx, totalNota.toDouble(), listTrxJual, 2)
        }

        binding.inBottom?.TvSmdg!!.setOnClickListener {
            binding.inBottom?.EtSearchProduk!!.setText("${binding.inBottom?.EtSearchProduk!!.text.toString()}=")
            binding.inBottom?.EtSearchProduk!!.setSelection(binding.inBottom?.EtSearchProduk!!.length())
        }
        binding.inBottom?.EtSearchProduk!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if ((event!!.action == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)
                ) {
                    // Perform action on key press
//                    Toast.makeText(this@KasirMainActivity, "mantap", Toast.LENGTH_SHORT).show();
                    val item =
                        KasirHelper.splitTemporary(binding.inBottom?.EtSearchProduk!!.EtToString())
                    if (item.size in 1..3) {
                        //insert
                        when (item.size) {
                            2 -> {
                                println("nama ${item[0]}")
                                println("harga ${item[1]}")
                            }
                            3 -> {
                                println("nama ${item[0]}")
                                println("harga ${item[1]}")
                                println("qty ${item[2]}")
                                try {
                                    inQty = item[2].toInt()
                                } catch (e: NumberFormatException) {
                                    e.printStackTrace()
                                }
                                if (idProduk == 0) {
                                    //add to temp produk
                                    //check name produk temporary
                                    mviewModel.getCheckerProdukSearchTemporary(item[0])
                                    tipeTrx = 3
                                    inProdTempNamaProduk = item[0]
                                    inProdTempHargaProduk = item[1].toDouble()
                                    addProdukTemp(item[0], 0, item[1].toDouble())
                                } else {
                                    if (item[1].toDouble() <= hargaProduk) {
                                        addProdukTemp(item[0], idProduk, hargaProduk)
                                    } else {
                                        //update temp
                                        tipeTrx = 3
                                        addProdukTemp(item[0], idProduk, item[1].toDouble())
                                        //insert baru
                                        if (item[1].toDouble() > hargaProduk) {
                                            val mProdTemp = TrxSearchTemporary()
                                            mProdTemp.nama_produk = item[0].capitalize(Locale.ROOT)
                                            mProdTemp.harga = item[1].toDouble()
                                            mProdTemp.count = 0
                                            mProdTemp.expired = "-"
                                            mviewModel.registrationProdukTemp(mProdTemp)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    hideSoftKeyboard()
                    return true;
                }
                return false
            }
        })

        binding.inBottom!!.EtPotNota.OnTextChangedListener {
            if (it.isNotEmpty()) {
                if (binding.inBottom!!.EtPotNota.EtToString().isNotEmpty()) {
                    val subtotal =
                        (totalNota.toInt() - it.toInt())
                    potonganNota = it.toDouble()
                    binding.inBottom!!.TvTotalPembeliandiskon.text =
                        "${NumberUtils.convertNumber(subtotal.toString())}"
                } else {
                    binding.inBottom!!.TvTotalPembeliandiskon.text =
                        NumberUtils.convertNumber(totalNota)
//                    binding.EtHargaBeli.error = "Harap Isi"
                }
            } else {
                potonganNota = 0.0
                binding.inBottom!!.TvTotalPembeliandiskon.text =
                    NumberUtils.convertNumber(totalNota)
            }
        }

        binding.inBottom!!.EtBayar.OnTextChangedListener {
            if (it.isNotEmpty()) {
                if (binding.inBottom!!.EtBayar.EtToString().isNotEmpty()) {
                    val _total = totalNota.toInt() - potonganNota.toInt()
                    if (it.toInt() > _total) {
                        val subtotals = (it.toInt() - _total)
//                    potonganNota = subtotal.toDouble()
                        binding.inBottom!!.TvTotalKembalian.text =
                            NumberUtils.convertNumber(subtotals.toString())
                    }
                } else {
                    binding.inBottom!!.TvTotalKembalian.text = ""
                }
            } else {
//                potonganNota = totalNota.toDouble()
                binding.inBottom!!.TvTotalKembalian.text = ""
//                binding.TvTotalPembeliandiskon.text = NumberUtils.convertNumber(totalNota)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2) {
            if (data != null) {
                val barcode: String? = data.getStringExtra(BarcodeScannerActivity.BARCODE_TYPE)
                searchProduk(barcode!!)
            }
        }
    }

    private fun searchProduk(item: String) {
        mviewModel.loadProdukSearch()
        mviewModel.getProdukSearchItem(item)
    }

    private fun addProdukTemp(nmProduk: String, idProduk: Int, harga: Double) {
        val modelInsert = TrxPenjualanTemp()
        modelInsert.namaBarang = nmProduk
        modelInsert.id_pembelian = 0
        modelInsert.kd_trx_pembelian = "-"
        modelInsert.kd_trx_penjualan = kode_trx
        modelInsert.kdProduk = idProduk.toString()
        modelInsert.kd_satuan = 0
        modelInsert.harga = harga
        modelInsert.qty = inQty
        modelInsert.keterangan = "-"
        modelInsert.diskon = 0.0
        modelInsert.tipe = tipeTrx
        modelInsert.total = (harga * inQty).toFloat()
        mviewModel.addProdukTemp(modelInsert)
        binding.inBottom?.EtSearchProduk!!.setText("")
    }

    override fun observeChange() {
        observe(mviewModel.mTrigerSavetoTrxJual, ::isSuccesSave)
        observe(mviewModel.getProdukTempAll, ::loadTemporary)
        observe(mviewModel.getCheckProdukTemporary, ::checkTempProdukName)
        observe(mviewModel.successAddTempProduk, ::successAddTemp)
        observe(mviewModel.getTotalTrxPenjualan, ::loadTotalJual)
        observe(mviewModel.getCheckStokPembelian, ::cekstokPembelian)
    }
//var sisaStok=0
    private fun cekstokPembelian(t: List<TrxPembelian>) {
//        if (t.isNotEmpty()) {
//            val sisa = t[0].stok - qty
//            if (sisa < 0) { //apabila ada sisa qty  stok ke - 1 tidak cukup
//                sisaStok = 0
//            } else { //jika stok normal
//
//            }
//        }
    }

    private fun successAddTemp(successAdd: Boolean) {
        if (successAdd) {
            //

            Log.d("action", "successAddTemp: ")
        }
    }

    private fun checkTempProdukName(pName: List<TrxSearchTemporary>) {
        if (pName.isEmpty()) {
            //add to name
            val mProdTemp = TrxSearchTemporary()
            mProdTemp.nama_produk = inProdTempNamaProduk.capitalize(Locale.ROOT)
            mProdTemp.harga = inProdTempHargaProduk
            mProdTemp.count = 0
            mProdTemp.expired = "-"
            mviewModel.registrationProdukTemp(mProdTemp)
        } else {
            //apabila ada produk nama serupa
            //apakah harganya sama ?
            if (inProdTempHargaProduk < pName[0].harga) {
                //harga = pName[0].harga
            } else {
                //update harga temp
                Log.d("add", "TempProdukName: ${pName[0].harga}")
            }
        }
    }

    private fun loadTemporary(listData: List<TrxSearchTemp.TrxSearchAll>) {
        presenter.setAdapterItem(listData)
    }

    private fun isSuccesSave(isSuccess: Boolean) {
        if (isSuccess) {
            mviewModel.getListTrxPenjualanTmp(kode_trx)
        }
    }

    private fun loadTotalJual(any: List<TrxPenjualanTempDao.TrxPenjualanTemporary>) {
        val total_all = any[0].subtotal.toString().replace(".0", "")
        totalNota = total_all
        binding.inBottom!!.TvTotal.text = NumberUtils.convertNumber(total_all)
    }

    private fun loadListTrxJualTemp(itemListTrxTemp: List<TrxPenjualanTemp>) {
        listTrxJual!!.addAll(itemListTrxTemp)
        mAdapter!!.notifyDataSetChanged()
    }

    override fun searchProdukTemporary(s: String?) {
        mviewModel.getProdukSearchAll(s!!)
//        mviewModel.loadSearchTmp()
    }

    override fun selectedProduk(
        idProduk: Int,
        NamaProduk: String?,
        tipeProduk: Int,
        inHarga: Double
    ) {
        //insert to trxspenjualan
//        addProdukTemp(NamaProduk!!,idProduk,inHarga)
        this.idProduk = idProduk
        hargaProduk = inHarga
        tipeTrx = tipeProduk
    }

//    Print

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                PERMISSION_BLUETOOTH -> printBluetooth()
            }
        }
    }


    private var selectedDevice: BluetoothConnection? = null

    fun browseBluetoothDevice() {
        val bluetoothDevicesList = BluetoothPrintersConnections().list
        if (bluetoothDevicesList != null) {
            val items = arrayOfNulls<String>(bluetoothDevicesList.size + 1)
            items[0] = "Default printer"
            var i = 0
            for (device in bluetoothDevicesList) {
                items[++i] = device.device.name
            }
            val alertDialog: android.app.AlertDialog.Builder =
                android.app.AlertDialog.Builder(this)
            alertDialog.setTitle("Bluetooth printer selection")
            alertDialog.setItems(
                items
            ) { dialogInterface, i ->
                val index = i - 1
                selectedDevice = if (index == -1) {
                    null
                } else {
                    bluetoothDevicesList[index]
                }
//                    val button: Button = findViewById<View>(R.id.button_bluetooth_browse) as Button
//                    button.text = items[i]
            }
            val alert: android.app.AlertDialog? = alertDialog.create()
            alert!!.setCanceledOnTouchOutside(false)
            alert.show()
        }
    }

    private fun printBluetooth() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.BLUETOOTH
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.BLUETOOTH),
                PERMISSION_BLUETOOTH
            )
        } else {
            AsyncBluetoothEscPosPrint(this).execute(this.getAsyncEscPosPrinter(selectedDevice))
        }
    }

    /**
     * Asynchronous printing
     */
    @SuppressLint("SimpleDateFormat")
    fun getAsyncEscPosPrinter(printerConnection: DeviceConnection?): AsyncEscPosPrinter? {
        val format = SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss")
        val printer = AsyncEscPosPrinter(printerConnection, 203, 48f, 32)
        return printer.setTextToPrint(
            """
            [L]
            [C]<u><font size='big'>ORDER N°045</font></u>
            [L]
            [C]<u type='double'>${format.format(Date())}</u>
            [C]
            [C]================================
            [L]
            [L]<b>BEAUTIFUL SHIRT</b>[R]9.99€
            [L]  + Size : S
            [L]
            [L]<b>AWESOME HAT</b>[R]24.99€
            [L]  + Size : 57/58
            [L]
            [C]--------------------------------
            [R]TOTAL PRICE :[R]34.98€
            [R]TAX :[R]4.23€
            [L]
            [C]================================
            [L]
            [L]<u><font color='bg-black' size='tall'>Customer :</font></u>
            [L]Raymond DUPONT
            [L]5 rue des girafes
            [L]31547 PERPETES
            [L]Tel : +33801201456
            
            [C]<barcode type='ean13' height='10'>831254784551</barcode>
            [L]
            [C]<qrcode size='20'>http://www.developpeur-web.dantsu.com/</qrcode>
            
            """.trimIndent()
        )
    }
}