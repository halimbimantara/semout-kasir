package com.semout.framework.mvvm.data.local

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import com.semout.framework.mvvm.data.local.dao.ProdukDao
import com.semout.framework.mvvm.data.local.dao.TrxPenjualanTempDao
import com.semout.framework.mvvm.data.local.dao.TrxSearchTemp
import com.semout.framework.mvvm.data.model.local.db.*
import com.semout.framework.mvvm.utils.SingleLiveEvent
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit


class AppDbHelper(private val database: AppDatabase, private val compos: CompositeDisposable) :
    DbHelpers {
    override fun getAllUser(): LiveData<List<UserLocal>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.userDao().loadAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getUserById(id: Int): Observable<UserLocal> {
        TODO("Not yet implemented")
    }

    override fun getKategoriProduk(): LiveData<List<KategoriProduk>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.kategoriDao().loadAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getKategoriProdukById(id: Int): Observable<KategoriProduk> {
        TODO("Not yet implemented")
    }

    override fun deleteKategoriProdukById(id: Int): Observable<Boolean> {
        TODO("Not yet implemented")
    }

    override fun saveKategoriProduk(model: KategoriProduk) {
        compos.add(Observable.fromCallable { database.kategoriDao().insert(model) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun getKemasanProduk(): LiveData<List<Kemasan>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.kemasanDao().loadAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getKemasanProdukById(id: Int): Observable<Kemasan> {
        TODO("Not yet implemented")
    }

    override fun deleteKemasanProdukById(id: Int): Observable<Boolean> {
        TODO("Not yet implemented")
    }

    override fun saveKemasanProduk(model: Kemasan) {
        compos.add(Observable.fromCallable { database.kemasanDao().insert(model) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun getSatuanProduk(): LiveData<List<Satuan>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.satuanDao().loadAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getSatuanProdukById(id: Int): Observable<Satuan> {
        TODO("Not yet implemented")
    }

    override fun deleteSatuanProdukById(id: Int): Observable<Boolean> {
        TODO("Not yet implemented")
    }

    override fun saveSatuanProduk(model: Satuan) {
        compos.add(Observable.fromCallable { database.satuanDao().insert(model) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun getPelanggan(): Observable<List<Pelanggan>> {
        TODO("Not yet implemented")
    }

    override fun getPelangganById(id: Int): Observable<Pelanggan> {
        TODO("Not yet implemented")
    }

    override fun deletePelangganById(id: Int): Observable<Boolean> {
        TODO("Not yet implemented")
    }

    override fun savePelanggan(model: Pelanggan): Observable<Boolean> {
        TODO("Not yet implemented")
    }

    override fun getProduk(): LiveData<List<Produk>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.produkDao().loadAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getProdukTemp(namaProd: String): LiveData<List<TrxSearchTemporary>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.trxSearchTempDao().searchProduk("'%${namaProd}%'")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getProdukTempV2(namaProd: String): Observable<List<TrxSearchTemporary>> {
        return database.trxSearchTempDao().searchProdukv2("%${namaProd}%").toObservable()
    }

    override fun getProdukAll(namaProd: String): Observable<List<TrxSearchTemp.TrxSearchAll>> {
        return database.trxSearchTempDao().searchProdukAll("%${namaProd}%").toObservable()
    }

    override fun getListProdukTemp(limit: Int, offset: Int): Observable<List<TrxSearchTemporary>> {
        return database.trxSearchTempDao().showallPagination()
    }

    override fun getCheckerProdukTempV2(namaProd: String): Single<List<TrxSearchTemporary>> {
        return database.trxSearchTempDao().searchProdukv2("%${namaProd}%")
    }

    override fun getProdukAll(): LiveData<List<TrxSearchTemporary>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.trxSearchTempDao().loadAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getProdukById(id: Int): Observable<Produk> {
        TODO("Not yet implemented")
    }

    override fun deleteProdukById(id: Int): Observable<Boolean> {
        TODO("Not yet implemented")
    }

    override fun deleteProdukTempById(id: Int): Completable {
        return database.trxSearchTempDao().deleteProdukByID(id)
    }

    override fun saveProduk(model: Produk) {
        compos.add(Observable.fromCallable { database.produkDao().insert(model) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun getProdukSearch(namaProd: String): LiveData<List<Produk>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.produkDao().loadAllByName(namaProd)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getProdukSearchv2(namaProd: String): Observable<List<Produk>> {
        return database.produkDao().loadAllByNamev2(namaProd).toObservable()
    }

    override fun getSuplier(): Observable<List<MasterSuplier>> {
        return database.suplierDao().loadAll().toObservable()
    }

    override fun getSuplierById(id: Int): Observable<MasterSuplier> {
        TODO("Not yet implemented")
    }

    override fun deleteSuplierById(id: Int): Observable<Boolean> {
        TODO("Not yet implemented")
    }

    override fun saveSuplier(model: MasterSuplier) {
        compos.add(Observable.fromCallable { database.suplierDao().insert(model) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun getTrxPembelian(kd_produk: String): Observable<List<TrxPembelian>> {
        return database.trxpembelianDao().getCekStokPembelian(kd_produk).toObservable()
    }

    override fun getTotalTrxPembelianTmp(kd_trx: String): Observable<List<TrxPembelianTemp>> {
        return database.trxpembelianTmpDao().loadTotalAllTemp(kd_trx).toObservable()
    }

    override fun getHargaTerakhhirPembelianTmp(kd_trx: String): LiveData<List<TrxPembelian>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.trxpembelianDao().getHargaLast(kd_trx)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getInfoStok(): LiveData<List<ProdukDao.InfoStok>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.produkDao().loadInfoStok()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getInfoStok(search: String): LiveData<List<ProdukDao.InfoStok>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.produkDao().loadInfoStok("%${search}%")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getLastTrxPembelianByIdProduk(
        idProduk: Int,
        kd_barcode: String
    ): Observable<List<TrxPembelian>> {
        return database.trxpembelianDao().getLastTrxPembelianByProduk(idProduk).toObservable()
    }

    override fun getKodeTrxJual(): String {
        TODO("Not yet implemented")
    }

    override fun addTemporaryProduk(produk: TrxSearchTemporary): Completable {
        return database.trxSearchTempDao().insertRx(produk)
    }

    override fun getProdukJual(search: String): LiveData<List<ProdukDao.InfoStokJual>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.produkDao().searchProduk(search)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getProdukJualBYCateg(search: Int): LiveData<List<Produk>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.produkDao().searchProdukCategory(search)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun AddProdukJualTmp(model: TrxPenjualanTemp) {
        compos.add(Observable.fromCallable { database.trxpenjualanTmpDao().insert(model) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun getProdukJualTmp(kd_trx: String): Observable<List<TrxPenjualanTemp>> {
        return database.trxpenjualanTmpDao().loadAll(kd_trx).toObservable()
    }

    override fun getTrxPenjualanTmp(kd_trx: String): Observable<List<TrxPenjualanTemp>> {
        return database.trxpenjualanTmpDao().loadTrxByKodeTrx(kd_trx).toObservable()
    }

    override fun getTotalTrxJualTmp(kd_trx: String): LiveData<List<TrxPenjualanTempDao.TrxPenjualanTemporary>> {
        return LiveDataReactiveStreams.fromPublisher(
            database.trxpenjualanTmpDao().loadTotalAllTemp(kd_trx)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
        )
    }

    override fun getTotalTrxJualTmpV2(kd_trx: String): Observable<List<TrxPenjualanTempDao.TrxPenjualanTemporary>> {
        return database.trxpenjualanTmpDao().loadTotalAllTempV2(kd_trx)
    }

    override fun getQtyJualTmp(
        kd_trx: String,
        name: String
    ): Observable<TrxPenjualanTempDao.TrxQty> {
        return database.trxpenjualanTmpDao().selectByKodeAndName(kd_trx, name).toObservable()
    }

    override fun deleteProdukJualTmp(id: Int) {
        compos.add(Observable.fromCallable { database.trxpenjualanTmpDao().deleteById(id) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun deleteProdukJualTmpByKodeAndName(kd_trx: String, name: String) {
        compos.add(Observable.fromCallable {
            database.trxpenjualanTmpDao().deleteByKodeAndName(kd_trx, name)
        }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun deleteAllProdukJualTmp() {
        compos.add(Observable.fromCallable { database.trxpenjualanTmpDao().deleteAll() }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun savePenjualanMaster(masterJual: MasterPenjualan) {
        compos.add(Observable.fromCallable { database.masterPenjualanDao().insert(masterJual) }
            .subscribeOn(Schedulers.computation())
            .subscribe()
        )
    }

    override fun savePenjualanTrx(trxJual: TrxPenjualan) {
        TODO("Not yet implemented")
    }

    override fun getTrxPembelianTmp(kd_trx: String): Observable<List<TrxPembelianTemp>> {
        return database.trxpembelianTmpDao().loadAllTemp(kd_trx).toObservable()

    }

    override fun saveTrxPembelianTmp(model: TrxPembelianTemp) {
        compos.add(Observable.fromCallable { database.trxpembelianTmpDao().insert(model) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun editTrxPembelianTmp(kdTrx: String, model: TrxPembelianTemp) {
        compos.add(Observable.fromCallable {
            database.trxpembelianTmpDao().updateTrxPembelian(model)
        }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun saveTrxTempPembelian(kdTrx: String) {
//        database.trxpembelianTmpDao().saveTempToTrx(kdTrx)
        compos.add(Observable.fromCallable {
            database.trxpembelianTmpDao().saveTempToTrx(kdTrx)
        }
            .delay(500, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun updateQty(qty: Int, id: Int) {
        compos.add(Observable.fromCallable { database.trxpenjualanTmpDao().updateQty(qty, id) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun deleteTrxPembelianTmp(id: Int) {
        compos.add(Observable.fromCallable { database.trxpembelianTmpDao().deleteById(id) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun deleteAllTrxPembelianTmp() {
        compos.add(Observable.fromCallable { database.trxpembelianTmpDao().deleteAll() }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    override fun updateStokPembelian(stokLast: Int, idPembelian: Int): Completable {
        return database.trxpembelianDao().updateStokPEmbelian(stokLast, idPembelian)
    }

    override fun saveMasterPembelian(model: MasterPembelian) {
        compos.add(Observable.fromCallable { database.masterPembelianDao().insert(model) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

}