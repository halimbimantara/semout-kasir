package com.semout.framework.mvvm.ui.produk.viewmodel

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.semout.framework.mvvm.core.BaseViewModel
import com.semout.framework.mvvm.data.model.local.db.*
import com.semout.framework.mvvm.data.repository.Repository
import com.semout.framework.mvvm.utils.NumberUtils
import com.semout.framework.mvvm.utils.ext.addTo
import com.semout.framework.mvvm.utils.rx.SchedulerProvider
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


class PembelianViewModel(
    private val schedulerProvider: SchedulerProvider,
    private val mainRepository: Repository
) : BaseViewModel() {
    val mTrigerSuplier: MutableLiveData<Boolean> = MutableLiveData()
    val mTrigerProduk: MutableLiveData<Boolean> = MutableLiveData()
    val mTriger: MutableLiveData<Boolean> = MutableLiveData()
    val mTrigerKemasan: MutableLiveData<Boolean> = MutableLiveData()
    val mTrigersatuan: MutableLiveData<Boolean> = MutableLiveData()
    val mTrigerSave: MutableLiveData<Boolean> = MutableLiveData()

    var pembelian_temp: MutableLiveData<List<TrxPembelianTemp>> = MutableLiveData()
    var total_pembelian_temp: LiveData<List<TrxPembelianTemp>>? = null


    var _totalTrxpembelian_temp: MutableLiveData<List<TrxPembelianTemp>> = MutableLiveData()
    var totalTrx_pembelian_temp: LiveData<List<TrxPembelianTemp>> = _totalTrxpembelian_temp

    val kategori: LiveData<List<KategoriProduk>> = Transformations.switchMap(mTriger) {
        mainRepository.getKategoriProduk()
    }
    val satuan: LiveData<List<Satuan>> = Transformations.switchMap(mTrigersatuan) {
        mainRepository.getSatuan()
    }
    val kemasan: LiveData<List<Kemasan>> = Transformations.switchMap(mTrigerKemasan) {
        mainRepository.getKemasan()
    }
    private var _trxPembelianByProduct: MutableLiveData<List<TrxPembelian>> = MutableLiveData()
    var trxPembelianByProduct: LiveData<List<TrxPembelian>> = _trxPembelianByProduct

    val suplier: MutableLiveData<List<MasterSuplier>> = MutableLiveData()
    fun getTrxPembelian(kd: String) {
        mainRepository.getTrxBeliTmp(kd)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeWith(object : DisposableObserver<List<TrxPembelianTemp>>() {
                override fun onNext(t: List<TrxPembelianTemp>) {
                    pembelian_temp.value = t
                }

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError: ${e.message}")
                }

                override fun onComplete() {
                    Log.d(TAG, "onComplete")
                }
            }).addTo(compositeDisposable)
    }


    fun getLastTrxPembelian(idProduk: Int) {
        mainRepository.getLastTrxPembelianByIdProduk(idProduk, "-")
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeWith(object : DisposableObserver<List<TrxPembelian>>() {
                override fun onNext(response: List<TrxPembelian>) {
                    _trxPembelianByProduct.postValue(response)
                }

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError: ${e.message}")
                }

                override fun onComplete() {
                    Log.d(TAG, "onComplete")
                }
            }).addTo(compositeDisposable)
    }


    fun setKodeTrxPembelian(kd: String) {
        mainRepository.setTrxPembelian(kd)
    }

    fun getSuplier(): LiveData<List<MasterSuplier>> {
        return suplier
    }

    fun getTotalTrxPembelian(kd_trx: String) {
        mainRepository.getTotalTrxBeliTmp(kd_trx)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeWith(object : DisposableObserver<List<TrxPembelianTemp>>() {
                override fun onNext(response: List<TrxPembelianTemp>) {
                    if (!response.isNullOrEmpty()) {
                        Log.i("lastTrxBeli", "Total pembelian ${response[0].harga}")
                    }
                    _totalTrxpembelian_temp.postValue(response)
                }

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError: ${e.message}")
                }

                override fun onComplete() {
                    Log.d(TAG, "onComplete")
                }
            }).addTo(compositeDisposable)
    }

    fun getLoadSuplier() {
        mainRepository.getSuplier()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeWith(object : DisposableObserver<List<MasterSuplier>>() {
                override fun onNext(t: List<MasterSuplier>) {
                    suplier.value = t
                }

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError: ${e.message}")
                }

                override fun onComplete() {
                    Log.d(TAG, "onComplete")
                }
            }).addTo(compositeDisposable)
    }

    val produk: LiveData<List<Produk>> = Transformations.switchMap(mTrigerProduk) {
        mainRepository.getProduk()
    }

    @JvmName("getProduk1")
    fun getProduk(): LiveData<List<Produk>> {
        return produk
    }

    fun getTempPembelian(): LiveData<List<TrxPembelianTemp>> {
        return pembelian_temp
    }

    fun getTotalTempPembelian(): LiveData<List<TrxPembelianTemp>>? {
        return total_pembelian_temp
    }

    fun loadProduk() {
        mTrigerProduk.value = true
    }

    fun loadSuplier() {
        mTrigerSuplier.value = true
    }

    fun loadKategori() {
        mTriger.value = true
    }

    @SuppressLint("CheckResult")
    fun selesai(
        kd_trx_beli: String,
        idSuplier: Int,
        potNota: Double,
        totalNota: Double,
        tglPembelian: String
    ) {
        val m_pembelian = MasterPembelian()
        m_pembelian.id_suplier = idSuplier
        m_pembelian.diskon_nota = potNota
        m_pembelian.total_pembelian = potNota
        m_pembelian.kd_trx_pembelian = kd_trx_beli
        m_pembelian.tgl_pembelian_suplier = ""
        m_pembelian.tgl_pembelian = tglPembelian
        mainRepository.addPembelian(m_pembelian)
        mainRepository.selesaiTrx(kd_trx_beli)
        //delay
        Completable.timer(7, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
            .subscribe {
                mainRepository.deleteTrxPembelian()
                //finish
            }
    }

    fun hapusPembelian(IdPembelian: Int) {
        mainRepository.delPembelianTmp(IdPembelian)
    }

    fun editPembelian(
        idPembelian: Int,
        kd_trx_beli: String,
        idProduk: Int,
        nmProduk: String,
        idSuplier: Int,
        qty: Int,
        hargabeli: Float,
        potNominal: String,
        potPersen: String,
    ) {
        val model = TrxPembelianTemp()
        model.id = idPembelian
        model.kd_trx_pembelian = kd_trx_beli
        model.kdProduk = idProduk.toString()
        model.namaBarang = nmProduk
        model.qty = qty
        model.harga = hargabeli
        model.diskon_nominal = if (potPersen.isNotEmpty()) potPersen.toInt() else 0
        model.total = hargabeli * qty
        model.diskon_nominal = if (potNominal.isNotEmpty()) potNominal.toInt() else 0
//        mainRepository.u(model)
        mTrigerSave.postValue(true)
    }

    fun addPembelianTemp(
        kd_trx_beli: String,
        idProduk: Int,
        nmProduk: String,
        idSuplier: Int,
        qty: Int,
        hargabeli: Float,
        potNominal: String,
        potPersen: String,
    ) {
        val diskon_persen_info =
            if (potPersen.toDouble() > 0)
                NumberUtils.presentasePotHarga(
                    potPersen.toDouble(),
                    hargabeli.toDouble()
                )
            else 0.0
        val model = TrxPembelianTemp()
        model.kd_trx_pembelian = kd_trx_beli
        model.kdProduk = idProduk.toString()
        model.namaBarang = nmProduk
        model.qty = qty
        model.stok = qty
        model.harga = hargabeli
        model.diskon_persen = if (potPersen.isNotEmpty()) potPersen.toInt() else 0
        model.tot_diskon =
            if (potNominal.isNotEmpty()) diskon_persen_info + potNominal.toDouble() else 0.0
        model.total = hargabeli * qty
        model.diskon_nominal = if (potNominal.isNotEmpty()) potNominal.toInt() else 0
        mainRepository.createPembelianTmp(model)
        mTrigerSave.postValue(true)
    }
}