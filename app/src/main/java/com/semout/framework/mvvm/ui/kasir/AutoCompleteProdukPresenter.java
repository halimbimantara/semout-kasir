package com.semout.framework.mvvm.ui.kasir;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.otaliastudios.autocomplete.AutocompletePresenter;
import com.otaliastudios.autocomplete.RecyclerViewPresenter;
import com.semout.framework.mvvm.R;
import com.semout.framework.mvvm.data.local.dao.TrxSearchTemp;
import com.semout.framework.mvvm.data.model.local.db.TrxSearchTemporary;
import com.semout.framework.mvvm.utils.NumberUtils;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteProdukPresenter extends RecyclerViewPresenter<TrxSearchTemp.TrxSearchAll> {

    @SuppressWarnings("WeakerAccess")
    protected Adapter adapter;
    private List<TrxSearchTemp.TrxSearchAll> marrModels;
    private SearchProdukListener listener;
    private Context mContext;

    @SuppressWarnings("WeakerAccess")
    public AutoCompleteProdukPresenter(@NonNull Context context, List<TrxSearchTemp.TrxSearchAll> model, SearchProdukListener mlSearchProdukListener) {
        super(context);
        this.mContext=context;
        this.marrModels = model;
        this.listener=mlSearchProdukListener;
    }

    public void setAdapterItem(List<TrxSearchTemp.TrxSearchAll> list){
//        TrxSearchTemporary attr=new TrxSearchTemporary();
//        attr.nama_produk="=";
//        attr.count=0;
//        attr.expired="no";
//        attr.harga=0;
//        list.add(list.size(),attr);
        adapter.setData(list);
        adapter.notifyDataSetChanged();
    }

    @NonNull
    @Override
    protected AutocompletePresenter.PopupDimensions getPopupDimensions() {
        AutocompletePresenter.PopupDimensions dims = new AutocompletePresenter.PopupDimensions();
        dims.width = ViewGroup.LayoutParams.MATCH_PARENT;
        dims.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        return dims;
    }

    @NonNull
    @Override
    protected RecyclerView.Adapter instantiateAdapter() {
        adapter = new Adapter();
        return adapter;
    }

    @Override
    protected void onQuery(@Nullable CharSequence query) {
        List<TrxSearchTemp.TrxSearchAll> all = new ArrayList<>();
        if (TextUtils.isEmpty(query)) {
            adapter.setData(all);
        } else {
            query = query.toString().toLowerCase();
            listener.searchProdukTemporary(query.toString());
            Log.d("cari ", "onQuery: "+query);
//            List<TrxSearchTemporary> list = new ArrayList<>();
//            for (TrxSearchTemporary u : all) {
//                if (u.nama_produk.toLowerCase().contains(query) ||
//                        u.nama_produk.toLowerCase().contains(query)) {
//                    list.add(u);
//                }
//            }
//            adapter.setData(list);
//            Log.e("UserPresenter", "found " + list.size() + " users for query " + query);
        }
        adapter.notifyDataSetChanged();
    }

    protected class Adapter extends RecyclerView.Adapter<Adapter.Holder> {

        private List<TrxSearchTemp.TrxSearchAll> data;

        @SuppressWarnings("WeakerAccess")
        protected void setData(@Nullable List<TrxSearchTemp.TrxSearchAll> data) {
            this.data = data;
        }

        @Override
        public int getItemCount() {
            return (isEmpty()) ? 1 : data.size();
        }

        @NonNull
        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new Holder(LayoutInflater.from(getContext()).inflate(R.layout.item_search_temp, parent, false));
        }

        private boolean isEmpty() {
            return data == null || data.isEmpty();
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull Holder holder, int position) {
            if (isEmpty()) {
                holder.fullname.setText("Produk Tidak Ditemukan");
                holder.username.setText("Sorry!");
                holder.close.setVisibility(View.GONE);
                holder.root.setOnClickListener(null);
                return;
            }
            final TrxSearchTemp.TrxSearchAll produkTemp = data.get(position);
            holder.fullname.setText(produkTemp.nama_produk);
            holder.fullname.setTextColor(mContext.getResources().getColor(R.color.blue));
            holder.username.setText("Rp." + NumberUtils.INSTANCE.convertNumber(Double.toString(produkTemp.harga).replace(".0","")));
            if (produkTemp.tipe == 1){
                holder.close.setVisibility(View.GONE);
            }else{
                holder.close.setVisibility(View.VISIBLE);
            }
            holder.root.setOnClickListener(v -> {
                dispatchClick(produkTemp);
                listener.selectedProduk(produkTemp.id,produkTemp.nama_produk,1,produkTemp.harga);
            });

        }

        @SuppressWarnings("WeakerAccess")
        protected class Holder extends RecyclerView.ViewHolder {
            private View root;
            private TextView fullname;
            private TextView username;
            private ImageView close;

            Holder(View itemView) {
                super(itemView);
                root = itemView;
                close = itemView.findViewById(R.id.delete);
                fullname = itemView.findViewById(R.id.fullname);
                username = itemView.findViewById(R.id.username);
            }
        }
    }

    public interface SearchProdukListener{
        void searchProdukTemporary(String s);
        void selectedProduk(int idProduk,String NamaProduk,int tipeProduk,double harga);
    }
}