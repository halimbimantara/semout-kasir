package com.semout.framework.mvvm.ui.produk.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.semout.framework.mvvm.data.model.local.db.MasterSuplier
import java.util.*


class SuplierAdapter(
    private val mcontext: Context,
    @LayoutRes private val layoutResource: Int,
    private var suplier: List<MasterSuplier?>
) :
    ArrayAdapter<MasterSuplier>(mcontext, layoutResource, suplier) {
    private var items: List<MasterSuplier?> = suplier

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(p0: Int): MasterSuplier? {
        return items.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        // Or just return p0
        return items[p0]!!.id_suplier.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context)
            .inflate(layoutResource, parent, false) as TextView
        view.text = items[position]!!.nama_suplier
        return view
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(
                charSequence: CharSequence?,
                filterResults: FilterResults
            ) {
                suplier = filterResults.values as List<MasterSuplier>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase(Locale.getDefault())

                val filterResults = FilterResults()
                filterResults.values = if (queryString == null || queryString.isEmpty())
                    items
                else
                    items.filter {
                        it!!.nama_suplier.toLowerCase().contains(queryString)
                    }
                return filterResults
            }
        }
    }
}