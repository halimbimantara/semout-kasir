package com.semout.framework.mvvm.ui.splash.intro

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.ui.usermanagement.login.activity.LoginActivity
import com.semout.framework.mvvm.utils.ProjectUtils

class IntroActivity : AppCompatActivity(), OnPageChangeListener {
    var mViewPager: ViewPager? = null
    var buttonSign: Button? = null
    var mResources = intArrayOf(
        R.drawable.img_slider_first,
        R.drawable.ic_slider_default,
        R.drawable.ic_slider_default
    )
    private var mAdapter: AppIntroPagerAdapter? = null
    private var viewPagerCountDots: LinearLayout? = null
    private var dotsCount = 0
    private lateinit var dots: Array<ImageView?>
    private var mContext: Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ProjectUtils.Fullscreen(this@IntroActivity)
        setContentView(R.layout.activity_intro)
        mContext = this@IntroActivity
        buttonSign = findViewById<View>(R.id.tombolstar) as Button
        buttonSign!!.setOnClickListener { view: View? ->
            val i = Intent(this@IntroActivity, LoginActivity::class.java)
            startActivity(i)
        }
        removeNotif()
        mViewPager = findViewById<View>(R.id.viewpager) as ViewPager
        viewPagerCountDots = findViewById<View>(R.id.viewPagerCountDots) as LinearLayout
        mAdapter = AppIntroPagerAdapter(this@IntroActivity, mContext, mResources)
        mViewPager!!.adapter =  mAdapter
        mViewPager!!.currentItem = 0
        mViewPager!!.setOnPageChangeListener(this)
        setPageViewIndicator()
    }

    @SuppressLint("ClickableViewAccessibility", "UseCompatLoadingForDrawables")
    private fun setPageViewIndicator() {
        Log.d("###setPageViewIndicator", " : called")
        dotsCount = mAdapter!!.count
        dots = arrayOfNulls(dotsCount)
        for (i in 0 until dotsCount) {
            dots[i] = ImageView(mContext)
            dots[i]!!.setImageDrawable(resources.getDrawable(R.drawable.nonselecteditem_dot))
            val params = LinearLayout.LayoutParams(
                25,
                25
            )
            params.setMargins(4, 20, 4, 0)
            dots[i]!!.setOnTouchListener { v: View?, event: MotionEvent? ->
                mViewPager!!.currentItem = i
                true
            }
            viewPagerCountDots!!.addView(dots[i], params)
        }
        dots[0]!!.setImageDrawable(resources.getDrawable(R.drawable.selecteditem_dot))
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onPageSelected(position: Int) {
        Log.e("###onPageSelected, pos ", position.toString())
        for (i in 0 until dotsCount) {
            dots[i]!!.setImageDrawable(resources.getDrawable(R.drawable.nonselecteditem_dot))
        }
        dots[position]!!.setImageDrawable(resources.getDrawable(R.drawable.selecteditem_dot))
        if (position + 1 == dotsCount) {
        } else {
        }
    }

    override fun onPageScrollStateChanged(state: Int) {}
    fun scrollPage(position: Int) {
        mViewPager!!.currentItem = position
    }

    override fun onBackPressed() {
        clickDone()
    }

    fun clickDone() {
        AlertDialog.Builder(this, R.style.DialogStyle)
            .setIcon(R.mipmap.ic_launcher)
            .setTitle(getString(R.string.app_name))
            .setMessage(getString(R.string.exit))
            .setPositiveButton(getString(R.string.yes)) { dialog, which ->
                dialog.dismiss()
                finish()
            }
            .setNegativeButton(getString(R.string.no)) { dialog, which -> dialog.dismiss() }
            .show()
    }

    private fun removeNotif() {
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(0)
    }
}