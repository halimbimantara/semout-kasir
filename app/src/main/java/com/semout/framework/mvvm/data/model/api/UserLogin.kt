package com.semout.framework.mvvm.data.model.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserLogin {
    @Expose
    @SerializedName("profil_img")
    var profil_img: String? = null

    @Expose
    @SerializedName("first_name")
    var fullname: String? = null

    @Expose
    @SerializedName("roleid")
    var roleid: String? = null

    @Expose
    @SerializedName("phone")
    var phone: String? = null

    @Expose
    @SerializedName("email")
    var email: String? = null

    @Expose
    @SerializedName("username")
    var username: String? = null

    @Expose
    @SerializedName("id")
    var id: String? = null
}