package com.semout.framework.mvvm.ui.usermanagement.register.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.data.model.ResponseHandling
import com.semout.framework.mvvm.databinding.RegisterScreenBinding
import com.semout.framework.mvvm.ui.settings.activity.SettingAlamatActivity
import com.semout.framework.mvvm.ui.usermanagement.register.RegisterViewModel
import com.semout.framework.mvvm.utils.Utils.getImeiDevice
import com.semout.framework.mvvm.utils.ext.EtToString
import com.semout.framework.mvvm.utils.ext.gone
import com.semout.framework.mvvm.utils.ext.observe
import com.semout.framework.mvvm.utils.ext.visible
import org.koin.android.viewmodel.ext.android.viewModel

class RegisterActivity : BaseActivity() {
    private val registerViewModel by viewModel<RegisterViewModel>()
    private lateinit var binding: RegisterScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = RegisterScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        actionUi()
        val idevice = getImeiDevice()
        Log.i("register", "onCreate: $idevice")
    }

    private fun actionUi() {
        binding.BtnCreateAkun.setOnClickListener {
            post_register()
        }
    }

    private fun post_register() {
        val in_fullname = binding.EtFullName.EtToString()
        val in_email = binding.EtEmail.EtToString()
        val in_uname = binding.EtUsernameName.EtToString()
        val in_pwd = binding.EtPassword.EtToString()
        val in_phone = binding.EtNoHp.EtToString()

        val info_error = "Harap isi dengan lengkap"
        if (in_fullname.isEmpty()) {
            binding.EtFullName.error = info_error
        } else if (in_email.isEmpty()) {
            binding.EtEmail.error = info_error
        } else if (in_uname.isEmpty()) {
            binding.EtUsernameName.error = info_error
        } else if (in_pwd.isEmpty()) {
            binding.EtPassword.error = info_error
        } else if (in_phone.isEmpty() && in_phone.length < 10) {
            binding.EtPhone.error = info_error
        } else {
            registerViewModel.register(in_fullname, in_email, in_phone, in_uname, in_pwd)
        }
    }

    private fun isLoading(loading: Boolean) {
        if (loading) {
            binding.BtnCreateAkun.gone()
        } else {
            binding.BtnCreateAkun.visible()
        }
    }

    override fun observeChange() {
        observe(registerViewModel.loading, ::isLoading)
        observe(registerViewModel.errorMessage, ::showSnackbarError)
        observe(registerViewModel.successMessage, ::showSnackbarMessage)
        observe(registerViewModel.responseData, ::handlingRegister)
    }

    private fun handlingRegister(responseHandling: ResponseHandling) {
        if (responseHandling.success) {
            finish()
        }
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, RegisterActivity::class.java)
        }
    }
}

