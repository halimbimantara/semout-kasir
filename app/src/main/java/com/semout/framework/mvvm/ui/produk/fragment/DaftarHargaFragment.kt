package com.semout.framework.mvvm.ui.produk.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.data.model.local.db.Produk
import com.semout.framework.mvvm.databinding.FragmentTabProdukBinding
import com.semout.framework.mvvm.databinding.ItemProdukBinding
import com.semout.framework.mvvm.ui.kasir.BarcodeScannerActivity
import com.semout.framework.mvvm.ui.produk.features.produk.AddProdukActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.ProdukViewModel
import com.semout.framework.mvvm.utils.NumberUtils
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File

class DaftarHargaFragment : BaseFragment() {
    override fun observeChange() {}
    private val mviewModel by viewModel<ProdukViewModel>()
    lateinit var binding: FragmentTabProdukBinding
    var ibarcode: String = ""
    var listProduk: ArrayList<Produk?>? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabProdukBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionUi()
        initUi()
        initData()
    }

    private fun initData() {
        mviewModel.getProduk()
        mviewModel.loadProduk()
        listProduk = ArrayList()
        var mAdapter =
            object :
                GenericAdapter<Produk?, ItemProdukBinding?>(
                    requireContext(),
                    listProduk!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_produk
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String {
                    return "Produk"
                }

                @SuppressLint("SetTextI18n")
                override fun onBindData(
                    model: Produk?,
                    position: Int,
                    dataBinding: ItemProdukBinding?
                ) {
                    val requestOptions = RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                        .skipMemoryCache(true)

//                    Glide.with(requireContext())
//                        .load(File(model!!.gambar))
//                        .placeholder(R.drawable.image_placeholder)
//                        .apply(requestOptions)
//                        .transition(DrawableTransitionOptions.withCrossFade())
//                        .transform(CircleCrop())
//                        .into(dataBinding!!.imageViewAvatar)

                    dataBinding!!.textViewUserName.text = model?.namaProduk
                    dataBinding.textViewUserEmail.text =
                        "Rp.${NumberUtils.convertNumber(model?.hg_jual.toString().replace(".0", ""))}"
                }

                override fun onItemClick(model: Produk?, position: Int) {
                }


            }
        binding.recyclerView.adapter = mAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext(),LinearLayoutManager.VERTICAL,false)
        mviewModel.getProduk().observe(requireActivity(), {
            listProduk!!.clear()
            listProduk!!.addAll(it!!)
            mAdapter.notifyDataSetChanged()
        })
    }

    private fun initUi() {

    }

    private fun actionUi() {
        binding.BtnAddProduk.setOnClickListener {
            val i = Intent(requireContext(), AddProdukActivity::class.java)
            startActivity(i)
        }

        binding.imvBarcode.setOnClickListener {
            val x = Intent(requireContext(), BarcodeScannerActivity::class.java)
            startActivityForResult(x, 2)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === 2) {
            val barcode: String? = data?.getStringExtra(BarcodeScannerActivity.BARCODE_TYPE)
            ibarcode = barcode ?: ""
            binding.etSearch.setText(barcode)
        }
    }
}