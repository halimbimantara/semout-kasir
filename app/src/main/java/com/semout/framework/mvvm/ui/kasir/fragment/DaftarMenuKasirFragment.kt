package com.semout.framework.mvvm.ui.kasir.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.data.model.local.db.KategoriProduk
import com.semout.framework.mvvm.data.model.local.db.Produk
import com.semout.framework.mvvm.data.model.local.db.TrxPenjualanTemp
import com.semout.framework.mvvm.databinding.FragmentIconKasirBinding
import com.semout.framework.mvvm.databinding.ItemKategoriFilterBinding
import com.semout.framework.mvvm.databinding.ItemProdukMenuProduckKasirBinding
import com.semout.framework.mvvm.ui.kasir.BarcodeScannerActivity
import com.semout.framework.mvvm.ui.kasir.viewmodels.KasirViewModel
import com.semout.framework.mvvm.ui.produk.features.produk.AddProdukActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.ProdukViewModel
import com.semout.framework.mvvm.utils.KasirHelper
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import com.semout.framework.mvvm.utils.ext.EtToString
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import kotlin.math.roundToInt

class DaftarMenuKasirFragment : BaseFragment() {
    private val mViewModel by viewModel<ProdukViewModel>()
    private val mViewModelKasir by sharedViewModel<KasirViewModel>()
    private val mViewModelKasirOnly by viewModel<KasirViewModel>()
    private var listKategori: ArrayList<KategoriProduk?>? = null
    lateinit var binding: FragmentIconKasirBinding
    var ibarcode: String = ""
    var listProduk: ArrayList<Produk?>? = null
    var kd_trxJual: String = ""
    private var inQty: Int = 1
    private var nameBarang: String = ""
    private var barangHarga: Double = 0.0
    private var barangId: Int = 0

    lateinit var mAdapter: GenericAdapter<Produk?, ItemProdukMenuProduckKasirBinding?>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentIconKasirBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionUi()
        initUi()
        initData()
        kategory()
    }

    override fun observeChange() {}
    private fun initData() {
        kd_trxJual = KasirHelper.getKodeTrxPembelian()
//        mViewModel.getProduk()
//        mViewModel.loadProduk()
        listProduk = ArrayList()
        mAdapter =
            object :
                GenericAdapter<Produk?, ItemProdukMenuProduckKasirBinding?>(
                    requireContext(),
                    listProduk!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_produk_menu_produck_kasir
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String? {
                    return "Produk"
                }

                @SuppressLint("SetTextI18n")
                override fun onBindData(
                    model: Produk?,
                    position: Int,
                    dataBinding: ItemProdukMenuProduckKasirBinding?
                ) {
                    val requestOptions = RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                        .skipMemoryCache(true)

                    Glide.with(requireContext())
                        .load(File(model!!.gambar))
                        .placeholder(R.drawable.image_placeholder)
                        .apply(requestOptions)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(dataBinding!!.imageViewIcon)

                    dataBinding.textViewName.text = model.namaProduk
//                    dataBinding.textViewUserEmail.text = model.hg_jual.toString().replace(".0", "")
                }

                override fun onItemClick(model: Produk?, position: Int) {
                    Log.d("click", "onTab Click = $itemCount")

                    Log.d("click", "onTab Click = $kd_trxJual && ${model!!.namaProduk}")

                    nameBarang = model.namaProduk
                    barangHarga = model.hg_jual
                    barangId = model.id

//                    mViewModelKasir.hapusProductJualTempByKodeAndName(kd_trxJual, model!!.namaProduk)
                    mViewModelKasirOnly.getQtyTrxTempV2(kd_trxJual,model.namaProduk)


                }
            }
        val display = requireActivity().windowManager.defaultDisplay
        val outMetrics = DisplayMetrics()
        display.getMetrics(outMetrics)
        val density = resources.displayMetrics.density
        val dpWidth: Float = outMetrics.widthPixels / density
        val columns = (dpWidth / 170).roundToInt()

        binding.recyclerView.adapter = mAdapter
        binding.recyclerView.layoutManager =
            GridLayoutManager(requireContext(), columns)
        mViewModel.getProduk().observe(requireActivity(), {
            listProduk!!.clear()
            listProduk!!.addAll(it!!)
            mAdapter.notifyDataSetChanged()
        })

    }

    private fun initUi() {

        mViewModelKasirOnly.qtyTemp.observe(requireActivity(),{
            if (it.count > 0) {
                inQty = it.qty + 1
                mViewModelKasirOnly.updateProductJualTemp(inQty,it.id)
                Log.d("Qty","Add Qty = $inQty")
//                mViewModelKasir.hapusProductJualTempByKodeAndName(kd_trxJual, model!!.namaProduk)
            } else {
                inQty = 1
                Log.d("Qty","Add Qty 0 = $inQty")
                addProdukTemp(nameBarang,barangId,barangHarga,inQty)
            }
        })

    }

    private fun kategory() {
        mViewModelKasirOnly.getKategori()
        mViewModelKasirOnly.loadKategori()
        listKategori = ArrayList()
        val mAdapter2 =
            object :
                GenericAdapter<KategoriProduk?, ItemKategoriFilterBinding>(
                    requireContext(),
                    listKategori
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_kategori_filter
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String? {
                    return "Kategori"
                }

                override fun onBindData(
                    model: KategoriProduk?,
                    position: Int,
                    dataBinding: ItemKategoriFilterBinding
                ) {
                    dataBinding.titleKategori.text = model!!.nama_kategori
                }

                override fun onItemClick(model: KategoriProduk?, position: Int) {
                    mViewModelKasirOnly.getProdukSearchByCategory(model!!.id)
                    mViewModelKasirOnly.getProdukSearchByCateg()?.observe(requireActivity(),{
                        listProduk!!.clear()
                        listProduk!!.addAll(it!!)
                        mAdapter.notifyDataSetChanged()
                    })
                    Log.d("Qty", "Add Qty = ${model!!.id}")
                }


            }
        binding.rvKategori.adapter = mAdapter2
        binding.rvKategori.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        mViewModelKasirOnly.getKategori().observe(requireActivity(), {
            listKategori!!.clear()
            listKategori!!.addAll(it!!)
            mAdapter2.notifyDataSetChanged()
        })
    }

    private fun actionUi() {
        binding.BtnAddProduk.setOnClickListener {
            val i = Intent(requireContext(), AddProdukActivity::class.java)
            startActivity(i)
        }

        binding.imvBarcode.setOnClickListener {
            val x = Intent(requireContext(), BarcodeScannerActivity::class.java)
            startActivityForResult(x, 2)
        }

        binding.buttonCari.setOnClickListener {
            if (binding.etSearch.EtToString().isNotEmpty()) {
                mViewModel.getProdukSearch(binding.etSearch.EtToString())
                mViewModel.getProdukSearch()?.observe(requireActivity(), {
                    listProduk!!.clear()
                    listProduk!!.addAll(it!!)
                    mAdapter.notifyDataSetChanged()
                })
            } else {
                mViewModel.getProduk()
                mViewModel.getProduk().observe(requireActivity(), {
                    listProduk!!.clear()
                    listProduk!!.addAll(it!!)
                    mAdapter.notifyDataSetChanged()
                })
            }

        }


    }
    fun addProdukTemp(nmProduk: String, idProduk: Int, harga: Double,countQty:Int) {

        mViewModelKasir.kdTrx = kd_trxJual
        val modelInsert = TrxPenjualanTemp()
        modelInsert.namaBarang = nmProduk
        modelInsert.id_pembelian = 0
        modelInsert.kd_trx_pembelian = "-"
        modelInsert.kd_trx_penjualan = kd_trxJual
        modelInsert.kdProduk = idProduk.toString()
        modelInsert.kd_satuan = 0
        modelInsert.harga = harga
        modelInsert.qty = countQty
        modelInsert.keterangan = "-"
        modelInsert.diskon = 0.0
        modelInsert.total = (harga * countQty).toFloat()
        mViewModelKasir.addProdukTemp(modelInsert)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2) {
            val barcode: String? = data?.getStringExtra(BarcodeScannerActivity.BARCODE_TYPE)
            ibarcode = barcode ?: ""
            binding.etSearch.setText(barcode)
        }
    }
}