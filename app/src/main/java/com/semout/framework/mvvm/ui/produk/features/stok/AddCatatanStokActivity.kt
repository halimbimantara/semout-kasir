package com.semout.framework.mvvm.ui.produk.features.stok

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.ui.produk.features.pembelian.AddPembelianActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.KemasanViewModel
import kotlinx.android.synthetic.main.add_kemasan_produk_screen.*
import org.koin.android.viewmodel.ext.android.viewModel

class AddCatatanStokActivity : BaseActivity() {
    private val mviewModel by viewModel<KemasanViewModel>()

    private val id_produk by lazy {
        intent.getIntExtra(EXTRA_KD_PRODUK, 0)
    }

    companion object {
        const val EXTRA_KD_PRODUK = "extra_kode_produk"
        fun newIntent(
            context: Context,
            idProduk: Int,
        ): Intent {
            val intent =
                Intent(context, AddCatatanStokActivity::class.java)
                    .putExtra(EXTRA_KD_PRODUK, idProduk)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_catatan_stok_screen)
        initUi()
        action()
        observeChange()
    }

    private fun initUi() {
//        textLabelToolbar.setText(getString(R.string.label_kemasan_produk))
    }

    fun action() {
        BackBtn.setOnClickListener {
            finish()
        }

    }

    override fun observeChange() {
    }
}