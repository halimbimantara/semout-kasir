/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.semout.framework.mvvm.data.model.local.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "trx_search_temporary")
public class TrxSearchTemporary {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "nama_produk")
    public String nama_produk;

    @ColumnInfo(name = "harga")
    public double harga;

    @ColumnInfo(name = "count")
    public int count;

    @ColumnInfo(name = "expired")
    public String expired;



}
