package com.semout.framework.mvvm.data.local.dao;
/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.semout.framework.mvvm.data.model.local.db.TrxPenjualanTemp;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by amitshekhar on 07/07/17.
 */

@Dao
public interface TrxPenjualanTempDao {

    @Delete
    void delete(TrxPenjualanTemp trxPenjualan);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TrxPenjualanTemp trxPenjualan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TrxPenjualanTemp> users);

    @Query("SELECT * FROM trx_penjualan_temp")
    Flowable<List<TrxPenjualanTemp>> loadAll();

    @Query("SELECT *,harga*qty as total FROM trx_penjualan_temp WHERE kd_trx_penjualan=:kd_trx")
    Single<List<TrxPenjualanTemp>> loadAll(String kd_trx);

    @Query("SELECT *,harga*qty as total FROM trx_penjualan_temp WHERE kd_trx_penjualan=:kd_trx")
    Single<List<TrxPenjualanTemp>> loadTrxByKodeTrx(String kd_trx);

    @Query("SELECT id,nama_barang,kd_produk,kd_satuan,harga,qty,diskon,total,SUM(total) as subtotal FROM trx_penjualan_temp WHERE kd_trx_penjualan=:kode_trx limit 0,1")
    Flowable<List<TrxPenjualanTemporary>> loadTotalAllTemp(String kode_trx);

    @Query("SELECT id,nama_barang,kd_produk,kd_satuan,harga,qty,diskon,total,SUM(total) as subtotal FROM trx_penjualan_temp WHERE kd_trx_penjualan=:kode_trx limit 0,1")
    Observable<List<TrxPenjualanTemporary>> loadTotalAllTempV2(String kode_trx);

    @Query("SELECT qty , id , count(id) as count , nama_barang, harga FROM trx_penjualan_temp where kd_trx_penjualan = :kodeTrx and nama_barang = :NameBrg limit 1")
    Single<TrxQty> selectByKodeAndName(String kodeTrx , String NameBrg );

    @Query("UPDATE trx_penjualan_temp SET qty = :qty , total = (:qty * harga)  WHERE id=:idPemb")
    void updateQty(int qty , int idPemb);

    @Query("DELETE FROM trx_penjualan_temp")
    void deleteAll();

    @Query("DELETE FROM trx_penjualan_temp where id=:idPemb")
    void deleteById(int idPemb);

    @Query("DELETE FROM trx_penjualan_temp where kd_trx_penjualan = :kodeTrx and nama_barang = :NameBrg")
    void deleteByKodeAndName(String kodeTrx , String NameBrg );

    class TrxPenjualanTemporary{
        public int id;
        public String kd_produk;
        public String nama_barang;
        public double harga;
        public int qty;
        public double diskon;
        public float total;
        public float subtotal;
        public int kd_satuan;
    }

    class TrxQty{
        public int qty;
        public int id;
        public int count;
        public String nama_barang;
        public double harga;
    }
}
