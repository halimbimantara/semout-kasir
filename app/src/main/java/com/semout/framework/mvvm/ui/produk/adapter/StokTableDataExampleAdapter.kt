package com.semout.framework.mvvm.ui.produk.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.semout.framework.mvvm.data.local.dao.ProdukDao
import de.codecrafters.tableview.toolkit.LongPressAwareTableDataAdapter
import java.text.NumberFormat

class StokTableDataExampleAdapter(
    context: Context?,
    data: List<ProdukDao.InfoStok>?,
    tableView: SortableCarTableView?
) : LongPressAwareTableDataAdapter<ProdukDao.InfoStok>(context, data, tableView) {
    companion object {
        private const val TEXT_SIZE = 14
        private val PRICE_FORMATTER = NumberFormat.getNumberInstance()
    }
    private fun renderString(value: String): View {
        val textView = TextView(context)
        textView.text = value
        textView.setPadding(20, 10, 20, 10)
        textView.textSize = TEXT_SIZE.toFloat()
        return textView
    }

    override fun getDefaultCellView(rowIndex: Int, columnIndex: Int, parentView: ViewGroup?): View {
        val stok: ProdukDao.InfoStok? = getRowData(rowIndex)
        var renderedView: View? = null
        when (columnIndex) {
            0 -> renderedView = renderString(stok!!.nm_produk)
            1 -> renderedView = renderString(stok!!.stok.toString())
            2 -> renderedView = renderString(stok!!.max_stok.toString())
            3 -> renderedView = renderString(stok!!.min_stok.toString())
            4 -> renderedView = renderString(stok!!.letak_rak)
        }
        return renderedView!!
    }

    override fun getLongPressCellView(
        rowIndex: Int,
        columnIndex: Int,
        parentView: ViewGroup?
    ): View {
        var renderedView: View? = null
        when (columnIndex) {
            1 -> {
            }
            else -> renderedView = getDefaultCellView(rowIndex, columnIndex, parentView)
        }
        return renderedView!!
    }
}