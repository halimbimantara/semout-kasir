package com.semout.framework.mvvm.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.semout.framework.mvvm.core.BaseViewModel
import com.semout.framework.mvvm.data.repository.Repository
import com.semout.framework.mvvm.utils.NetworkHelper
import com.semout.framework.mvvm.utils.rx.SchedulerProvider

class SplashViewModel(
    private val mainRepository: Repository,
    private val schedulerProvider: SchedulerProvider,
    private val networkHelper: NetworkHelper
) : BaseViewModel() {
    private val _isLogin = MutableLiveData<Boolean>()
    val isLoged: LiveData<Boolean>
        get() = _isLogin

    init {
//        fetchUsersLoged()
    }

    private fun fetchUsersLoged() {
        if (mainRepository.getUname().isNotEmpty()){
            _isLogin.postValue(true)
        }else{
            _isLogin.postValue(false)
        }
    }
}